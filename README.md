# Quantitative Cyber-Metric

Quantitative cybermetric, named Cyber-Physical Energy System Real-time Security Visibility (CPES-RTSV) metric, designed to provide a numerical score to the cyber and physical status of an operating cyber-physical energy system (CPES). This cybermetric, via an easy-to-interpret score, provides operating observability of individual components in a CPES that can be interpreted by both human operators or sophisticated control systems.

The cybermetric is computed by receiving measurement factors from three domains: 1) Electrical, 2) IT, and 3) Graph-theory. Within these domains, factors are also classified according to the environments they affect: 1) Physical, 2) Cyber, and 3) Network. After all factors are received and computed, they are combined using a Multi-criteria decision-making (MCDM) approach called Choquet Integral. The fmtools C++ package is used to facilitate this computation.

## Installation Requirements:

### Requirements to run CyberMetric (using pip3)
-----
- numpy (>= 1.19.4)

- pandapower (>= 0.51.2)

- pandas (>= 1.1.4)

- matplotlib (>= 3.3.3)

- sympy (>= 1.7)

- networkx (>= 2.5)

- numba (>= 0.51.2) (required to speed up calculations and remove warnings for solver)
* To install numba via pip3, you may need to install llvm-10 (or higher version)

- sudo apt install llvm-10


### Requirements for Plotting
-----

- pip3 install plotly (>= 4.14.3)

- sudo apt-get install python3-igraph

- pip3 install IPython (>= 7.16.1)

- pip3 install geopy (>= 2.1.0)


### Requirements C++ (needed to run fmtools_3.0)
-----

1. g++/gcc (>= 7.5.0)

2. sudo apt install swig 

(needed to run ./compile.sh)


## Instructions to Run:

### Compile and Cleaning:

1. The computation of the cybermetric, using fmtools_3.0, requires the linking of C++ and Python codes. To do this, just execute the `compile.sh`. 
    - $ ./`compile.sh`

2. After running and testing, you can remove the compiled files by executing the file `removed_compiled_files.sh`
    - $ ./`remove_compiled_files.sh`

### Description of Files:

#### Factors calculation/computation:
    - `factors_electrical.py`: Factors related to Electrical domain. 
    - `factors_graphtheory.py`: Factors related to Network (graph-theory) domain.
    - `factors_it.py`: Factors related to IT domain.
    - `factors_capability.py`: Factors related to Capability domain (WIP).

#### Fuzzy Measure computation:
    - `fuzzy_measures.py`: Computes Lambda values and Fuzzy measures.

#### Python - C++ linking files (using SWIG):   
    - `fmtools.cpp`: Functions to be interfaced with fmtools.
    - `fmtools.h`: Header file of Functions to be interfaced with fmtools.
    - `fmtools.i`: Interface module C++-Python.

#### Test Files:   
    - `test_fmtools.py`: Test functions from `fuzzy_measures.py` and linked `fmtools`.
    - `test_individ_cybermetric.py`: Test functions from linked `fmtools`.
    - `test_sympy.py`: Test sympy.
    - `test_sc1_acopf_cybermetric_v1.py`: Test the use of the cybermetric in cyber-constrained ACOPF (no modification of problematic nodes).
    - `test_sc1_acopf_cybermetric_v2.py`: Test the use of the cybermetric in cyber-constrained ACOPF (modification of problematic nodes).


