//************************************************************************
//							"PARCER.CPP"
//
//	AUTHOR		:	Juan Esteban Monsalve Tob�n
//	DATE			:	January, 2005
//
//	DESCRIPTION	:	Here in is the code for the parcer class to 
//				grab input from the initialization files.
//
//
/*
  usage: ParcerDer P("inputfile.txt");
         P.start();
		std::vector<Variable>::const_iterator current;
		std::vector<Variable> v=parcer.GetVars();
		Variable temp;

	//look at every variable obtained by the parcer and check to see if it
	//matches any of the atributes. If it does then the atrivute is set to
	//the value given.
	for(current=v.begin(), temp = *current ;current != v.end(); current++)
	{
		temp = *current;

		if(temp == "power")
			M_POWER = (*current).to_double();
		else if(temp == "clusters") ...
*/
// 
//
//*************************************************************************

#include "parcer.h"

//_________________________________________________________________________
//
//CLASS COMMUNICATOR-------------------------------------------------------
//_________________________________________________________________________

Parcer::Parcer()
:filename("quasi.txt")
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		Parcer::Parcer()
	//
	//	DESCRIPTION: 
	//		Initializes the input file to "quasi.txt".
	//		
	//----------------------------------------------------------------------
}

Parcer::Parcer(const char *file)
:filename(file)
{
	//----------------------------------------------------------------------
	//	NAME:
	//		Parcer::Parcer(char* file)
	//
	//	DESCRIPTION:
	//		Initializes the input file to a given name.
	//		
	//----------------------------------------------------------------------
}

Parcer::~Parcer()
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		virtual Parcer::~Parcer()
	//
	//	DESCRIPTION: 
	//		closes the inputfile stream if it is still open.
	//		
	//----------------------------------------------------------------------

	if(input.is_open())
		input.close();
}

bool Parcer::init(char *file)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Parcer::init(char* file)
	//
	//	DESCRIPTION: 
	//		Tries to open an input stream from the input file. If the file
	//		does not exist it prints a warning to stdout.
	//
	//	PRE:
	//		There is a filename to try and open.
	//
	//	POST:
	//		The file is opened for input, or if the file does not exist then
	//		an error message is printed to the screen.
	//
	//----------------------------------------------------------------------

	if(input.is_open())
		input.close();

	input.open(file, ios::in);
	
	if(!input.is_open())
	{
		cout<<"could not open "<<file<<", using default values."<<endl;
	}

	return input.is_open();
}

bool Parcer::start()
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Parcer::start()
	//
	//	DESCRIPTION: 
	//		If the file is opened then this funciton starts to parce the 
	//		file. Else it just bails and returns false.
	//
	//	PRE:
	//		File has not been parced for input.
	//
	//	POST:
	//		The whole file is parced for input, if for some reason somthing
	//		goes wrong then the function return false. Else it returns true
	//		and by now the tokens or variables have been extracted from the
	//		file (valid once any way).
	//
	//	NOTES:
	//		If this function returns false then you should ignore any thing
	//		that was extracted from the file.
	//		
	//----------------------------------------------------------------------

	input.open(filename.c_str(), ios::in);
	
	if(!input.is_open())
	{
		cout<<"could not open "<<filename<<", using default values."<<endl;
	}
	else
		ProcessTokens();

	return input.is_open();
}

std::vector<Variable> Parcer::GetVars()
{
	//----------------------------------------------------------------------
	//	NAME:
	//		std::vector<Variable> Parcer::GetVars()
	//
	//	DESCRIPTION: 
	//		Returns a vector of type Varibles; which holds the varibles 
	//		extracted from the input file (quasi.txt or other).
	//		
	//----------------------------------------------------------------------

	return vars;
}

ostream& Parcer::operator<<(ostream& out)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		ostream& Parcer::operator<<(ostream& out)
	//
	//	DESCRIPTION: 
	//		overloaded operator which prints to a given output stream the 
	//		current contents of the parcer (contents of the variable vector).
	//		
	//----------------------------------------------------------------------

	if(!vars.empty())
	{
		std::ostream_iterator< Variable > output(out, "\n");
		std::copy(vars.begin(), vars.end(), output);
	}
	return out;
}

//Private Methods-------------------------------------------------------------
void Parcer::ProcessTokens()
{
	while(!input.eof())
	{	
		c = input.get();
		switch(c)
		{
			case '#' :	input.ignore(500, '\n');
						break;

			case '$' :	if(!Gettoken())
							input.ignore(500,'\n');						
						break;

			case ' ' : 
			case '\t':	
			case '\n':	
						break;

			default  :
						break;
		}
	}
}

bool Parcer::Gettoken()
{
	name = UNDEF;
	value = UNDEF;

	int state = 0;	

	for(buffc=0; buffc<sizeb && !input.eof();)
	{
		c = input.get();
		switch(c)
		{
			case ' ' :
			case '\t':
			case '=' :	if(!SetNamVal())
							return false;

			case '\n':	current = find(vars.begin(),vars.end(),Variable(name, value));

						if(current == vars.end())
							vars.push_back(Variable(name, value));
						else
							*current=Variable(name, value);	
						return true;

			default  :	buff[buffc] = c;
						buffc++;

		}
	}
	return true;
}

bool Parcer::SetNamVal()
{
	int state;

	for(state =0; state != -1;)
	{
		switch(state)
		{
			case 0:	if(name == UNDEF)
					{
						buff[buffc] ='\0';
						name = string(buff);
						buff[buffc] = c;
						buffc = 0;
					}

					if(name == "") //check for empty string
					{
						name = UNDEF;
						input.ignore(500, '\n');
						return false;
					}

					if(c=='=')
						state = 1;
					else if(c == ' ' || c == '\t')
						state = 3;
					else if( c == '\n')
					{
						input.putback('\n');
						state = -1;
					}
					else
						return false;
					break;
			
			case 3:	if(c == ' ' || c == '\t')
					{}
					else if( c == '\n')
					{
						input.putback('\n');
						state = -1;
					}
					else if(c=='=')
						state = 1;
					else
						return false;

					break;

					break;
			case 1:	if(c == ' ' || c == '\t')
					{}
					else if( c == '\n')
						state = -1;
					else
					{
						buff[buffc] = c;
						buffc++;
						state = 2;
					}
					break;

			case 2: if(c == ' ' || c == '\t' || c == '\n' || c == EOF)
					{
						buff[buffc] ='\0';
						value = string(buff);
						buff[buffc] = c;
						buffc = 0;
						state = -1;

						if(c == '\n')
							input.putback('\n');
					}
					else
					{
						buff[buffc] = c;
						buffc++;
						state = 2;
					}
					break;

			default:
					break;
		}

		//check for buffer over flow
		if(buffc>=sizeb)
			return false; 

		if(!input.eof())
			c = input.get();
	}
	return true;
}

ostream& operator<<(ostream &out,  Parcer& p)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		friend ostream& operator<<(ostream &out,  Parcer& p)
	//
	//	DESCRIPTION: 
	//		Friend overloaded operator which prints to a given output stream 
	//		current current contents of the parcer (contents of the variable 
	//		vector).
	//
	//----------------------------------------------------------------------

	if(!p.vars.empty())
	{
		std::ostream_iterator< Variable > output(out, "\n");
		std::copy(p.vars.begin(), p.vars.end(), output);
	}
	return out;
}


CParcerDer::CParcerDer(const char *file)
:Parcer(file), arraynum(0)
{}

void CParcerDer::ProcessTokens()
{
	arraynum=0;
	while(!input.eof())
	{
		c = input.get();
		switch(c)
		{
			case '#' :	input.ignore(500, '\n');
						break;

			case '$' :	if(!Gettoken())
							input.ignore(500,'\n');
						break;

			case '&' :	if(!GetArray())
							input.ignore(500,'\n');
						break;
			case 160:
			case ' ' :
			case '\t':
			case '\n':
						break;
			case 0xFF:
						goto L1;

			default  :
						break;
		}
	}
L1:;
}

int CParcerDer::size(){
	return arraynum;
}

const dVec& CParcerDer::GetVector(int i){

	if((i < arraynum) && (arraynum > 0))
	{
		return vecs[i];
	}

	return temp=0;
}

bool CParcerDer::GetArray()
{
	input >> temp;
	vecs.push_back(temp);
	arraynum++;
	return true;
}

/*bool CParcerDer::GetArray()
{
//	Variable v;
//	if(!Gettoken())	input.ignore(5000,'\n');

//	v=vars.back();
	switch(arraynum) {
	case 0:
		input >> m_inputvec;
		arraynum++;
		break;
	case 1:
		input >> m_inputvec1;
		arraynum++;
		break;
	case 2:
		input >> m_inputvec2;
		arraynum++;
		break;
	}
	return true;
}*/




//_________________________________________________________________________
//
//CLASS VARIABLE------------------------------------------------------------
//_________________________________________________________________________

Variable::Variable()
	:vname(UNDEF), value(UNDEF)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable()
	//
	//	DESCRIPTION: 
	//		This constructor sets the value and name of the nes varible to
	//		to UNDEF (undefined).
	//
	//		
	//----------------------------------------------------------------------
}

Variable::Variable(string vname_)
	:vname(vname_)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable(string name_)
	//
	//	DESCRIPTION: 
	//		Initializes the variable name to a given name and its value to
	//		undefined.
	//		
	//----------------------------------------------------------------------
}

Variable::Variable(string vname_=UNDEF, string value_=UNDEF)
	:vname(vname_), value(value_)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable(string vname_, string value_)
	//
	//	DESCRIPTION: 
	//		Initializes a values name and its value to a given name and 
	//		value.
	//		
	//----------------------------------------------------------------------
}

Variable::~Variable()
{
	//----------------------------------------------------------------------
	//	NAME:
	//		Variable::~Variable()
	//
	//	DESCRIPTION: 
	//		Does nothing.
	//		
	//----------------------------------------------------------------------
}

string Variable::GetName() const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		string Variable::GetName() const
	//
	//	DESCRIPTION: 
	//		Used to access the name of the variable
	//
	//	PRE:
	//		Nothing.
	//
	//	POST:
	//		Returns the name of the varible, or wether it has defined or not.
	//		
	//----------------------------------------------------------------------

	return vname;
}
	
string Variable::GetVal() const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		string Variable::GetVal() const
	//
	//	DESCRIPTION: 
	//		Used to access the value of the variable.
	//
	//	PRE:
	//		Nothing.
	//
	//	POST:
	//		returns a copy of the varible's  value.
	//		
	//----------------------------------------------------------------------

	return value;
}
	
bool Variable::operator=(const Variable x)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		string Variable::GetVal() const
	//
	//	DESCRIPTION: 
	//		Used to access the value of the variable.
	//
	//	PRE:
	//		Nothing.
	//
	//	POST:
	//		returns a copy of the varible's  value.
	//		
	//----------------------------------------------------------------------

	vname = x.GetName();
	value = x.GetVal();

	return true;
}

bool Variable::operator==(const Variable x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator= (const Variable x)
	//
	//	DESCRIPTION: 
	//		Assings the value and name of one variable to another.
	//
	//	PRE:
	//		Only works for assingment between Varibles objects
	//
	//	POST:
	//		At the end the varible on the left hand side of the equals sign
	//		is assigned to this variable.
	//
	//----------------------------------------------------------------------

	return vname == x.GetName();
}

bool Variable::operator==(const string& x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator==(const string& x)const
	//
	//	DESCRIPTION: 
	//		checks to see if a string is equal to the name of a varible.
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if the names are the same or false if they are not.
	//		
	//----------------------------------------------------------------------

	return vname == x;
}

bool Variable::operator==(const char *x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator==(const string& x)const
	//
	//	DESCRIPTION: 
	//		checks to see if a C string is equal to the name of a varible.
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if the names are the same or false if they are not.
	//		
	//----------------------------------------------------------------------

	return vname == string(x);
}

bool Variable::operator!=(const Variable x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const Variable x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a Variable on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//	
	//----------------------------------------------------------------------

	return vname != x.GetName();
}

bool Variable::operator!=(const string& x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const string& x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//		
	//----------------------------------------------------------------------

	return vname != x;
}

bool Variable::operator!=(const char *x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const char *x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//		
	//----------------------------------------------------------------------	

	 return vname != string(x);
}

bool Variable::operator<(const Variable x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const Variable x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than another varible's name. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a Variable on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the other varible's name.
	//		
	//----------------------------------------------------------------------

	return vname < x.GetName();
}

bool Variable::operator<(const string& x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const string& x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than the string. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the string.
	//		
	//----------------------------------------------------------------------

	return vname < x;
}

bool Variable::operator<(const char *x)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const char *x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than the C string. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the C string.
	//		
	//----------------------------------------------------------------------

	return vname < string(x);
}

double Variable::to_double() const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		double Variable::to_double() const
	//
	//	DESCRIPTION: 
	//		Takes the value in string form and transforms it to a double.
	//		
	//----------------------------------------------------------------------

	char *stopstring;
	return strtod(value.c_str(), &stopstring);		
}

int Variable::to_integer() const
{
	return atoi(value.c_str());
}

ostream& Variable::operator<<(ostream& out)const
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		ostream& Variable::operator<<(ostream& out)const
	//
	//	DESCRIPTION: 
	//		Used to print the variable name and value, using cout or another
	//		output stream.
	//		
	//----------------------------------------------------------------------

	out<<vname<<"="<< value<<endl;
	return out;
}
	
ostream& operator<<(ostream& out, const Variable& v)
{
	//----------------------------------------------------------------------
	//	NAME: 
	//		ostream& operator<<(ostream& out, const Variable& v)
	//
	//	DESCRIPTION: 
	//		Used to print the variable name and value, using cout or another
	//		output stream.
	//
	//	NOTE:
	//		Friend of the class varible.
	//		
	//----------------------------------------------------------------------

	out<<v.vname<<"="<< v.value;
	return out;
}







