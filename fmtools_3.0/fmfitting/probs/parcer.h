//************************************************************************
//							"PARCER.H"
//
//	AUTHOR		:	Juan Esteban Monsalve Tob�n
//	DATE		:	January, 2002
//
//	DESCRIPTION	:	Here in is the code for the parcer class 
//
//*************************************************************************

#ifndef PARCER_H
#define PARCER_H

//INCLUDED FILES***********************************************************
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdlib.h>

using std::ifstream;
using std::ios;
using std::cout;
using std::cin;
using std::endl;
//using std::sort;
using std::find;
using std::vector;
using std::string;
using std::ostream;


const int sizeb=500;

#include "mymath.h"


//CLASS DECLARATIONS********************************************************


//this constant string stands for undefined variable it is allocated to 
//a variable when it is not defined.

const string UNDEF = "UNDEF";

//CLASS DECLARATIONS********************************************************


class Variable{
//----------------------------------------------------------------------
//	NAME: 
//		Variable class
//
//	DESCRIPTION: 
//		This is a container class which stores a variable name and its
//		value. Both are stored as strings.
//		
//----------------------------------------------------------------------
public:
	Variable();
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable()
	//
	//	DESCRIPTION: 
	//		This constructor sets the value and name of the nes varible to
	//		to UNDEF (undefined).
	//
	//		
	//----------------------------------------------------------------------

	Variable(string name_);
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable(string name_)
	//
	//	DESCRIPTION: 
	//		Initializes the variable name to a given name and its value to
	//		undefined.
	//		
	//----------------------------------------------------------------------

	Variable(string vname_, string value_);
	//----------------------------------------------------------------------
	//	NAME: 
	//		Variable::Variable(string vname_, string value_)
	//
	//	DESCRIPTION: 
	//		Initializes a values name and its value to a given name and 
	//		value.
	//		
	//----------------------------------------------------------------------
	
	~Variable();
	//----------------------------------------------------------------------
	//	NAME:
	//		Variable::~Variable()
	//
	//	DESCRIPTION: 
	//		Does nothing.
	//		
	//----------------------------------------------------------------------

	friend ostream& operator<<(ostream& out, const Variable& v);
	//----------------------------------------------------------------------
	//	NAME: 
	//		friend ostream& operator<<(ostream& out, const Variable& v)
	//
	//	DESCRIPTION: 
	//		Friend of Variable class, it is used to pring a variable's name
	//		and value.
	//		
	//----------------------------------------------------------------------

	
	string GetName() const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string Variable::GetName() const
	//
	//	DESCRIPTION: 
	//		Used to access the name of the variable
	//
	//	PRE:
	//		Nothing.
	//
	//	POST:
	//		Returns the name of the varible, or wether it has defined or not.
	//		
	//----------------------------------------------------------------------

	string GetVal() const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string Variable::GetVal() const
	//
	//	DESCRIPTION: 
	//		Used to access the value of the variable.
	//
	//	PRE:
	//		Nothing.
	//
	//	POST:
	//		returns a copy of the varible's  value.
	//		
	//----------------------------------------------------------------------
	
	bool operator=(const Variable x);
	//----------------------------------------------------------------------
	//	NAME: 

	//		bool Variable::operator= (const Variable x)
	//
	//	DESCRIPTION: 
	//		Assings the value and name of one variable to another.
	//
	//	PRE:
	//		Only works for assingment between Varibles objects
	//
	//	POST:
	//		At the end the varible on the left hand side of the equals sign
	//		is assigned to this variable.
	//
	//----------------------------------------------------------------------

	bool operator==(const Variable x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator==(const Variable x)const
	//
	//	DESCRIPTION: 
	//		checks to see if another varible is the same as this one (e.g. do
	//		they share the same name.
	//
	//	PRE:
	//		There must be another varible object on the other side of the 
	//		equal sing.
	//
	//	POST:
	//		returns true if the names are the same or false if they are not.
	//		
	//----------------------------------------------------------------------
	
	bool operator==(const string& x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator==(const string& x)const
	//
	//	DESCRIPTION: 
	//		checks to see if a string is equal to the name of a varible.
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if the names are the same or false if they are not.
	//		
	//----------------------------------------------------------------------

	bool operator==(const char *x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator==(const string& x)const
	//
	//	DESCRIPTION: 
	//		checks to see if a C string is equal to the name of a varible.
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if the names are the same or false if they are not.
	//		
	//----------------------------------------------------------------------

	bool operator!=(const Variable x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const Variable x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a Variable on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//	
	//----------------------------------------------------------------------

	bool operator!=(const string& x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const string& x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//		
	//----------------------------------------------------------------------

	bool operator!=(const char *x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator!=(const char *x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if another varible's name is different to this 
	//		one's.
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		returns true if they are different or false if they are the same.
	//		
	//----------------------------------------------------------------------

	bool operator<(const Variable x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const Variable x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than another varible's name. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a Variable on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the other varible's name.
	//		
	//----------------------------------------------------------------------

	bool operator<(const string& x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const string& x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than the string. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a string on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the string.
	//		
	//----------------------------------------------------------------------

	bool operator<(const char *x)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Variable::operator<(const char *x)const
	//
	//	DESCRIPTION: 
	//		Checks to see if this variable's name has an alphabetical 
	//		possition less than the C string. (e.g. "abc" < "bcd").
	//
	//	PRE:
	//		There must be a C string on the other side of the equals sing.
	//
	//	POST:
	//		Returns true if this variable's name has an alphabetical 
	//		possition less than the C string.
	//		
	//----------------------------------------------------------------------

	double to_double() const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		double Variable::to_double() const
	//
	//	DESCRIPTION: 
	//		Takes the value in string form and transforms it to a double.
	//		
	//----------------------------------------------------------------------

	int to_integer() const;

	ostream& operator<<(ostream& out)const;
	//----------------------------------------------------------------------
	//	NAME: 
	//		ostream& Variable::operator<<(ostream& out)const
	//
	//	DESCRIPTION: 
	//		Used to print the variable name and value, using cout or another
	//		output stream.
	//		
	//----------------------------------------------------------------------
	
private:	
	string vname;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string vname
	//
	//	DESCRIPTION: 
	//		Stores the name of the variable, if UNDEF it means that the 
	//		variable is not defined.
	//
	//----------------------------------------------------------------------

	string value;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string value
	//
	//	DESCRIPTION: 
	//		Stores the value of the variable as a string if it needs to be
	//		converted to something else later on (e.g. a double) then the
	//		functions excist to do just that.
	//		
	//----------------------------------------------------------------------
};

ostream& operator<<(ostream& out, const Variable& v);
//----------------------------------------------------------------------
//	NAME: 
//		ostream& operator<<(ostream& out, const Variable& v)
//
//	DESCRIPTION: 
//		Used to print the variable name and value, using cout or another
//		output stream.
//	
//	NOTE:
//		Friend of the class varible.
//		
//----------------------------------------------------------------------



class Parcer  
{
//--------------------------------------------------------------------------
//	NAME:
//		Parcer Class
//
//	DATE:
//		January, 2002
//
//	DESCRIPTION:
//		This parcer was written with the purpose of traversing initialization
//		files for the Quasi classes and the AtomProb class, its task is to
//		identify the following structures on a perline basis:
//
//			'#' denotes as commented out line.
//			'$' denotes a variable declaration.
//			'$<variable><white space/nothing> = <white space/nothing> <value>' 
//				is a variable assignment.	
//
//		All varible declaration need to be separated by newline charecter. 
//		All ways use lower case letters.	
//
//--------------------------------------------------------------------------

public://-------------------------------------------------------------------
	
	Parcer();
	//----------------------------------------------------------------------
	//	NAME: 
	//		Parcer::Parcer()
	//
	//	DESCRIPTION: 
	//		Initializes the input file to "quasi.txt".
	//		
	//----------------------------------------------------------------------

	Parcer(const char* file);
	//----------------------------------------------------------------------
	//	NAME:
	//		Parcer::Parcer(char* file)
	//
	//	DESCRIPTION:
	//		Initializes the input file to a given name.
	//		
	//----------------------------------------------------------------------

	virtual ~Parcer();
	//----------------------------------------------------------------------
	//	NAME: 
	//		virtual Parcer::~Parcer()
	//
	//	DESCRIPTION: 
	//		closes the inputfile stream if it is still open.
	//		
	//----------------------------------------------------------------------

	bool init(char* file);
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Parcer::init(char* file)
	//
	//	DESCRIPTION: 
	//		Tries to open an input stream from the input file. If the file
	//		does not exist it prints a warning to stdout.
	//
	//	PRE:
	//		There is a filename to try and open.
	//
	//	POST:
	//		The file is opened for input, or if the file does not exist then
	//		an error message is printed to the screen.
	//
	//----------------------------------------------------------------------

	bool start();	
	//----------------------------------------------------------------------
	//	NAME: 
	//		bool Parcer::start()
	//
	//	DESCRIPTION: 
	//		If the file is opened then this funciton starts to parce the 
	//		file. Else it just bails and returns false.
	//
	//	PRE:
	//		File has not been parced for input.
	//
	//	POST:
	//		The whole file is parced for input, if for some reason somthing
	//		goes wrong then the function return false. Else it returns true
	//		and by now the tokens or variables have been extracted from the
	//		file (valid once any way).
	//
	//	NOTES:
	//		If this function returns false then you should ignore any thing
	//		that was extracted from the file.
	//		
	//----------------------------------------------------------------------

	std::vector<Variable> GetVars();
	//----------------------------------------------------------------------
	//	NAME:
	//		std::vector<Variable> Parcer::GetVars()
	//
	//	DESCRIPTION: 
	//		Returns a vector of type Varibles; which holds the varibles 
	//		extracted from the input file (quasi.txt or other).
	//		
	//----------------------------------------------------------------------

	ostream& operator<<(ostream& out);
	//----------------------------------------------------------------------
	//	NAME: 
	//		ostream& Parcer::operator<<(ostream& out)
	//
	//	DESCRIPTION: 
	//		overloaded operator which prints to a given output stream the 
	//		current contents of the parcer (contents of the variable vector).
	//		
	//----------------------------------------------------------------------

	friend ostream& operator<<(ostream &out,  Parcer& p);
	//----------------------------------------------------------------------
	//	NAME: 
	//		friend ostream& operator<<(ostream &out,  Parcer& p)
	//
	//	DESCRIPTION: 
	//		Friend overloaded operator which prints to a given output stream 
	//		current current contents of the parcer (contents of the variable 
	//		vector).
	//
	//----------------------------------------------------------------------

//protected://----------------------------------------------------------------

	virtual void ProcessTokens();
	//----------------------------------------------------------------------
	//	NAME: 
	//		virtual void Parcer::ProcessTokens()
	//
	//	DESCRIPTION: 
	//		
	//
	//	PRE:
	//
	//	POST:
	//
	//	NOTES:
	//		
	//----------------------------------------------------------------------

	virtual bool SetNamVal();
	//----------------------------------------------------------------------
	//	NAME: 
	//		virtual bool Parcer::SetNamVal()
	//
	//	DESCRIPTION: 
	//
	//	PRE:
	//
	//	POST:
	//
	//	NOTES:
	//		
	//----------------------------------------------------------------------
	
	virtual bool Gettoken();
	//----------------------------------------------------------------------
	//	NAME: 
	//		virtual bool Parcer::Gettoken()
	//
	//	DESCRIPTION: 
	//
	//	PRE:
	//
	//	POST:
	//
	//	NOTES:
	//		
	//----------------------------------------------------------------------

//private://------------------------------------------------------------------

	string filename;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string filename
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	ifstream input;
	//----------------------------------------------------------------------
	//	NAME: 
	//		ifstream input
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	string name;
	//----------------------------------------------------------------------
	//	NAME: 
	//		string name
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	string value;
	//----------------------------------------------------------------------
	//	NAME:
	//		string value
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	char buff[sizeb];
	//----------------------------------------------------------------------
	//	NAME: 
	//		char buff[sizeb]
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	char c;
	//----------------------------------------------------------------------
	//	NAME: 
	//		char c
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	int buffc;
	//----------------------------------------------------------------------
	//	NAME:
	//		int buffc
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	std::vector<Variable> vars;
	//----------------------------------------------------------------------
	//	NAME:
	//		std::vector<Variable> vars
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

	std::vector<Variable>::iterator current;
	//----------------------------------------------------------------------
	//	NAME: 
	//		std::vector<Variable>::iterator current
	//
	//	DESCRIPTION: 
	//		
	//----------------------------------------------------------------------

};

ostream& operator<<(ostream& out, const Parcer& p);
//----------------------------------------------------------------------
//  NAME: 
//		ostream& operator<<(ostream& out, const Parcer& p)
//
//  DESCRIPTION: 
//
//  PRE:
//
//  POST:
//
//  NOTES:
//      
//----------------------------------------------------------------------




class CParcerDer: public Parcer
{
public:
	CParcerDer (const char *file);

	virtual void ProcessTokens();

	int size();

	const dVec& GetVector(int i);

private:
	vector<dVec> vecs;
	dVec temp;
	int arraynum;

	bool	GetArray();
};

#endif
