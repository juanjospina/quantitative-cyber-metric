// fuzzymeasure.cpp : Defines the entry point for the console application.
#include "probs/parcer.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;
//#define WIN32


#include "../src/fuzzymeasuretools.h"
#include "../src/fuzzymeasurefit.h"



int D=3;		
string filename = "test", filename_o= "testout.txt", filename_o1="dummy", filename_o2="dummy";
dVec UpperData, LowerData, ULdata1, ULdata2, wData, XYData;
int cardordering=1, datanum=0;
int options=0, options1=0, interval=0, symmetric=0, additive=0;
double ornesshigh=1, ornesslow=0;
int Kadd=0,datanumtest=0;
int nooutput=0;
double KConst = 0.5;
 


void initVars(const char* filename1)
{
	CParcerDer parcer(filename1);

	bool r=parcer.start();
	if(r) {
		std::vector<Variable>::const_iterator current;
		std::vector<Variable> v=parcer.GetVars();
		Variable temp;


	//look at every variable obtained by the parcer and check to see if it
	//matches any of the atributes. If it does then the atrivute is set to
	//the value given.
	for(current=v.begin(), temp = *current ;current != v.end(); current++)
	{
		temp = *current;

		if(temp == "data_set") 	    filename = string((*current).GetVal());
		else
		if(temp == "output") 	    filename_o = (*current).GetVal();
		else
		if(temp == "test_data") 	    filename_o1 = (*current).GetVal();
		else
		if(temp == "test_dataout") 	    filename_o2 = (*current).GetVal();
		else
		if(temp == "cardordering") 	    cardordering = (*current).to_integer();
		else
		if(temp == "DataNumber") 	    datanum = (*current).to_integer();
		else
		if(temp == "DataNumbertest") 	    datanumtest = (*current).to_integer();
		else
		if(temp == "interval") 	    interval = (*current).to_integer();
		else
		if(temp == "symmetric") 	    symmetric = (*current).to_integer();
		else
		if(temp == "additive") 	    additive = (*current).to_integer();
		else
		if(temp == "options") 	    options = (*current).to_integer();
		else
		if(temp == "kadd") 			Kadd = (*current).to_integer();
		else
		if(temp == "nooutput") 			nooutput = (*current).to_integer();
		else
		if(temp == "options1") 	    options1 = (*current).to_integer();
		else
		if(temp == "ornesslow") 	    ornesslow = (*current).to_double();
		else
		if(temp == "ornesshigh") 	    ornesshigh = (*current).to_double();
		else
		if (temp == "kconst") 	    KConst = (*current).to_double();
		else
		if(temp == "Dimension")		{D = (*current).to_integer(); }
					else
			{}
	}

	ULdata1=parcer.GetVector(0);
	ULdata2=parcer.GetVector(1);

	}
}

double randone()
{
	return rand() / (RAND_MAX - 1.);
}


int main(int argc, char *argv[])
{
	int_64  i;
	int k;
	int flag = 0;
	int i1, j1;
	string  token;
	ofstream on, on1;
	double temp1=0;

	//if correct number of argument then usage stream is printed to stdout
	if(argc < 3){
		cerr<<"\n\nCorrect Usage: fmfitting -[f configfile]"<<endl;
//		goto L1;
		return 1;
	}
	//extract arguments
	for(i1=1; i1<argc; i1++){
		token = string(argv[i1]);
		if(token == "-f")
			filename = string(argv[++i1]);	//file where series in question is stored
		else	{}
	}
	initVars(filename.c_str());
	on.open(filename_o.c_str(), std::ios::out);

	goto L2;
	/*
L1:
	on.open("outmeasure1a.txt", std::ios::out);
	flag = 1;
	D = 15;
	datanum = 100;
	options = 9;
	Kadd = 6;

	srand(11);
	XYData.newsize(datanum*(D + 1));
	for (i1 = 0; i1 < datanum; i1++) {
		for (j1 = 0; j1 <= D; j1++) {
			XYData[(D + 1)*i1 + j1]=randone();
		}
	}

	*/
// we also have read the desired values of Shapley indices and another constraint
// the k-additivity (value k).
// we need to set up an LP problem to determine fuzzy measure which best fits the data


L2:

	int n=D;
	int_64 m;
	int flagparams1=0, flagparams2=0;

	if(additive) symmetric=1;

	if(!symmetric)	if (options>=12) Preparations_FM_marginal(n, &m, ((Kadd < n) ? Kadd + 1 : n)); else Preparations_FM(n,&m);
		 else m=n;





	double *w=new double[m];
	double *Mob=new double[m];
	double *dual=new double[m];
	double *w2=new double[m];

	ifstream inf, inf1;

	if (flag) goto L3;
	inf.open(filename.c_str(), std::ios::in);
// we have read datanum pairs (x,y), with x a vector of size Dimension and y a scalar
// we keep them in the array XYData

	if(!interval) {
		XYData.newsize(datanum*(n+1));
		k=0;
		for(i1=0;i1<datanum;i1++) {
			for(j1=0;j1<=n;j1++) {
				inf >> XYData[(n+1)*i1 + j1];
				if(inf.fail()) break;
			}
			if(inf.fail()) break;
			k++;
		}
	} else // interval
	{
		XYData.newsize(datanum*(n+2));
		k=0;
		for(i1=0;i1<datanum;i1++) {
			for(j1=0;j1<=n+1;j1++) {
				inf >> XYData[(n+2)*i1 + j1];
				if(inf.fail()) break;
			}
			if(inf.fail()) break;
			k++;
		}
	}
	inf.close();
	datanum=k;
L3:;

	double orness[2],temp;
	double* interlow=new double[m];
	double* interhigh=new double[m];
	double* shapleylow=new double[n];
	double* shapleyhigh=new double[n];

	double *shapley = new double[n];
	double *banzhaf = new double[n];
	double *inter = new double[m];
	double *interB = new double[m];

	orness[0]=ornesslow; orness[1]=ornesshigh;

// now check the validity of parameters 
	if(Kadd==0 || Kadd >n) Kadd=n;
	if(orness[0]<0 || orness[0]>1)orness[0]=0;
	if(orness[1]<0 || orness[1]>1)orness[1]=1;

	if(symmetric) goto symm1;

	if(ULdata1.dim() == 2*n) { // means Shapley values
		for(i=0;i<n;i++) { shapleylow[i]=ULdata1[i]; if(shapleylow[i]<0 || shapleylow[i]>1) shapleylow[i]=0;}
		for(i=0;i<n;i++) { shapleyhigh[i]=ULdata1[i+n]; if(shapleyhigh[i]<0 || shapleyhigh[i]>1) shapleyhigh[i]=1;}
		flagparams1=1;
	} else if(ULdata1.dim() == 2*m) { // means interaction indices
		for(i=0;i<m;i++) { interlow[i]=ULdata1[i]; if(interlow[i]<-1 || interlow[i]>1) interlow[i]=-1;}
		for(i=0;i<m;i++) { interhigh[i]=ULdata1[i+m]; if(interhigh[i]<-1 || interhigh[i]>1) interhigh[i]=1;}
		flagparams2=1;
	}
	if(ULdata2.dim() == 2*n && ULdata1.dim() != 2*n) { // means Shapley values
		for(i=0;i<n;i++) { shapleylow[i]=ULdata2[i]; if(shapleylow[i]<0 || shapleylow[i]>1) shapleylow[i]=0;}
		for(i=0;i<n;i++) { shapleyhigh[i]=ULdata2[i+n]; if(shapleyhigh[i]<0 || shapleyhigh[i]>1) shapleyhigh[i]=1;}
		flagparams1=1;
	} else if(ULdata2.dim() == 2*m && ULdata1.dim() != 2*m) { // means interaction indices
		for(i=0;i<m;i++) { interlow[i]=ULdata2[i]; if(interlow[i]<-1 || interlow[i]>1) interlow[i]=-1;}
		for(i=0;i<m;i++) { interhigh[i]=ULdata2[i+m]; if(interhigh[i]<-1 || interhigh[i]>1) interhigh[i]=1;}
		flagparams2=1;
	}

symm1:
	if(!nooutput){
		cout<<"Parameters are read and "<<datanum<<" data loaded from the file "<<filename<<endl;
		cout<<"Starting fitting process...";
	}

	int res;

	if(symmetric) goto symm2;



repeat:
// fit the data
	if(!interval) {
			if((options==1 || options==2 || options==3) && flagparams1)
				res=FuzzyMeasureFitLP(n,  m,  datanum,  Kadd, w,  &(XYData[0]), options, shapleylow , shapleyhigh, options1, orness); 
			else if((options==4 || options==5 || options==6) && flagparams2)
				res=FuzzyMeasureFitLP(n,  m,  datanum,  Kadd, w,  &(XYData[0]), options, interlow , interhigh, options1, orness); 
			else if (options==7)
				res = FuzzyMeasureFitLPMIP(n, m, datanum, Kadd, w, &(XYData[0]));
			else if (options == 8)
				res = FuzzyMeasureFitLP_relaxation(n, m, datanum, Kadd, w, &(XYData[0]));
			else if (options == 9)
				res = FuzzyMeasureFitLPStandard(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness);
			else if (options == 10)
				res = FuzzyMeasureFitLPKinteractiveMaxChains(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness, KConst);
			else if (options == 11)
				res = FuzzyMeasureFitLPKinteractive(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness, KConst);
			else if (options == 14)
				res = FuzzyMeasureFitLPKinteractiveAutoK(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness, KConst, 200);
			else if (options == 12)
				res = FuzzyMeasureFitLPKinteractiveMarginal(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness, KConst);
			else if (options == 13)
				res = FuzzyMeasureFitLPKinteractiveMarginalMaxChain(n, m, datanum, Kadd, w, &(XYData[0]), options, interlow, interhigh, options1, orness, KConst);

			else
				res = FuzzyMeasureFitLP(n, m, datanum, Kadd, w, &(XYData[0]), 0, NULL, NULL, options1, orness);
	} else // interval 
 {
/*			if((options==1 || options==2 || options==3) && flagparams1)
				res=FuzzyMeasureFitLPinterval(n,  m,  datanum,  Kadd, w,  &(XYData[0]), options, shapleylow , shapleyhigh, options1, orness); 
			else if((options==4 || options==5 || options==6) && flagparams2)
				res=FuzzyMeasureFitLPinterval(n,  m,  datanum,  Kadd, w,  &(XYData[0]), options, interlow , interhigh, options1, orness); 
			else
				res=FuzzyMeasureFitLPinterval(n,  m,  datanum,  Kadd, w,  &(XYData[0]), 0, NULL , NULL, options1, orness); 
				*/
	} 
	if(res !=1 && ((options1 & 0x4)== 0x4)) { // try without ordering constraints
		options1 -= 0x4;

	   if(!nooutput){
		 	cout<<endl<<"The output ordering preservation is inconsistent with outher constraints, and is removed... ";

			on<< "The output ordering preservation is inconsistent with outher constraints, and is removed... "<<endl;
		}
		goto repeat;
	}
	goto rescont;

symm2:

	if(additive) {
		options=1; options1=0;
	}

	if(!interval)
		res=FuzzyMeasureFitLPsymmetric(n,  datanum,  w,  &(XYData[0]), options, NULL , NULL, options1, orness);
	else 
		res=FuzzyMeasureFitLPsymmetricinterval(n,  datanum,  w,  &(XYData[0]), options, NULL , NULL, options1, orness);

rescont:
	if(res !=1 ) {

	  if(!nooutput){
		cout<<endl<<"The fitting problem cannot be solved, probably because the constraints are inconsistent. Try using less conditions."<<endl;
		cout<<"No output was produced."<<endl;

		on<<"The fitting problem cannot be solved, probably because the constraints are inconsistent. Try using less conditions."<<endl;
	  }
		goto quit;
	}


	  if(!nooutput){
		  cout<<" Finished."<<endl;
		  cout<<"Calculating various indices and printing them into the output file."<<endl;
	  }





if(symmetric) goto symm3;

// successful. Now we have w in cardinality ordering in Moebius representation, with padded 0
// next calculate Mobius in binary ordering
if (options <7)
	ConvertCard2Bit(Mob, w, m);
else
	for (i = 0; i<m; i++)  	Mob[i] = w[i];

	////
	if (options != 12)
		ConvertCard2Bit(w2, w, m);
	else
	for (i = 0; i<m; i++)  	w2[i] = w[i];


	if (options == 7 || options == 8 || options == 9 || options == 10 || options == 11 || options == 12 || options == 13 || options == 14 || options == 15)
		Mobius(w2, Mob, n, m); // calculates Moebius representation of v
	else
		// calculate the standard representation
		Zeta(Mob, w2, n, m);


	if(nooutput) { // no detailed output
		for(i=0;i<m;i++)  on<<w2[i]<<" "; 
		on<<endl;
		goto quit;
	}

	if (options == 7 || options == 8) on << "The resulting " << Kadd << "-maxitive fuzzy measure is the following:" << endl; else
	if (options >= 9) on << "The resulting " << Kadd << "-interactive fuzzy measure is the following:" << endl; else
		on << "The resulting " << Kadd << "-additive fuzzy measure is the following:" << endl;

	for(i=0;i<m;i++)  on<<w2[i]<<" \t{"<<ShowValue(i)<<"}"<<endl;

	on<<"Its Mobius representation is:"<<endl;
	for(i=0;i<m;i++)
		on<< Mob[i] << " \t{"<<ShowValue(i)<<"}"<<endl;

	on<<"The computed orness value is "<<Orness(Mob,n,m)<<endl;
	on<<"The computed Entropy  is "<<Entropy(w2,n,m)<<endl;

	on<<"The supplied and calculated output values are the following (in 3 columns):"<<endl;
	temp=0;

	if(!interval) {
		for(k=0;k<datanum;k++) {
			on<< XYData[(n+1)*k + n] << " "<<Choquet(&(XYData[(n+1)*k]),  w2,n,m )<<endl;
			temp += fabs(XYData[(n+1)*k + n] - Choquet(&(XYData[(n+1)*k]),  w2,n,m ));
		}
	} else
		for(k=0;k<datanum;k++) {
			on<< XYData[(n+2)*k + n]<< " "<<  XYData[(n+2)*k + n+1] << " "<<Choquet(&(XYData[(n+2)*k]),  w2,n,m )<<endl;
			temp1=XYData[(n+2)*k + n+0] - Choquet(&(XYData[(n+2)*k]),  w2,n,m );
			if(temp1<= 0) temp1=-(XYData[(n+2)*k + n+1] - Choquet(&(XYData[(n+2)*k]),  w2,n,m ));
			if(temp1<0) temp1=0;
			temp += temp1;
		}


	on<<"Difference predicted and observed "<<temp<<endl;

// now calculate and print all the indices


	on<<endl;
	on<<"Shapley values"<<endl;
    Shapley(w2, shapley,  n,  m);
	for(i1=0;i1<n;i1++) on<<shapley[i1]<<" "<<i1+1<<endl;
	on<<endl;

	on<<"Banzhaf values"<<endl;
	Banzhaf(w2, banzhaf,  n,  m);
	for(i1=0;i1<n;i1++) on<<banzhaf[i1]<<" "<<i1+1<<endl;
	on<<endl;


	on<<"Interaction indices (in binary ordering)"<<endl;
	Interaction(Mob,inter,m);
	for(i=0;i<m;i++)
		on<< inter[i] << " \t{"<<ShowValue(i)<<"}"<<endl;
	on<<endl;

	on << "Nonadditivity indices (in binary ordering)" << endl;
	NonadditivityIndex(w2, inter, n, m);
	for (i = 0; i<m; i++)
		on << inter[i] << " \t{" << ShowValue(i) << "}" << endl;
	on << endl;

	on << "Bipartition Shapley indices (in binary ordering)" << endl;
	BipartitionShapleyIndex(w2, inter, n, m);
	for (i = 0; i<m; i++)
		on << inter[i] << " \t{" << ShowValue(i) << "}" << endl;
	on << endl;

	on<<"This fuzzy measure is:"<<endl;
	if(IsMeasureSubadditive(w2,m)) 
		on<<"- subadditive"<<endl; else on<<"- not subadditive"<<endl;
	if(IsMeasureSuperadditive(w2,m)) 
		on<<"- superadditive"<<endl; else on<<"- not superadditive"<<endl;
	if(IsMeasureSubmodular(w2,m)) 
		on<<"- submodular"<<endl; else on<<"- not submodular"<<endl;
	if(IsMeasureSupermodular(w2,m)) 
		on<<"- supermodular"<<endl; else on<<"- not supermodular"<<endl;
	if(IsMeasureBalanced(w2,m)) 
		on<<"- balanced"<<endl; else on<<"- not balanced"<<endl;
	if(IsMeasureSymmetric(w2,n,m)) 
		on<<"- symmetric"<<endl; else on<<"- not symmetric"<<endl;

	on << "- kmaxitive for k=" << IsMeasureKMaxitive(w2, n, m) << endl;




	goto quit1;

symm3:
	if(!additive) {
	// we have OWA
	on<< "OWA weights are:"<<endl;
		for(i=0;i<n;i++)  on<<w[i]<<" \t{"<<i<<"}"<<endl;

	on<<"The computed orness value is "<<OrnessOWA(w,n)<<endl;
	on<<"Observed and predicted values "<<endl;

	temp=0;
	if(!interval) {
		for(k=0;k<datanum;k++) {
			on<< XYData[(n+1)*k + n] << " "<<OWA(&(XYData[(n+1)*k]),  w,n )<<endl;
			temp += fabs(XYData[(n+1)*k + n] - OWA(&(XYData[(n+1)*k]),  w,n ));
		}
	} else
		for(k=0;k<datanum;k++) {
			on<< XYData[(n+2)*k + n]<< " "<<  XYData[(n+2)*k + n+1] << " "<<OWA(&(XYData[(n+2)*k]),  w,n )<<endl;
			temp1=XYData[(n+2)*k + n+0] - OWA(&(XYData[(n+2)*k]),  w,n);
			if(temp1<= 0) temp1=-(XYData[(n+2)*k + n+1] - OWA(&(XYData[(n+2)*k]),  w,n));
			if(temp1<0) temp1=0;
			temp += temp1;
		}


	on<<"Difference predicted and observed "<<temp<<endl;

		on<<"This fuzzy measure is:"<<endl;
		on<<"- symmetric"<<endl;
	} else {// additive
		on<< "WAM weights are:"<<endl;
			for(i=0;i<n;i++)  on<<w[i]<<" \t{"<<i<<"}"<<endl;

	   on<<"Observed and predicted values "<<endl;

		temp=0;
		if(!interval) {
			for(k=0;k<datanum;k++) {
				on<< XYData[(n+1)*k + n] << " "<<WAM(&(XYData[(n+1)*k]),  w,n )<<endl;
				temp += fabs(XYData[(n+1)*k + n] - WAM(&(XYData[(n+1)*k]),  w,n ));
			}
		} else
			for(k=0;k<datanum;k++) {
				on<< XYData[(n+2)*k + n]<< " "<<  XYData[(n+2)*k + n+1] << " "<<WAM(&(XYData[(n+2)*k]),  w,n )<<endl;
				temp1=XYData[(n+2)*k + n+0] - WAM(&(XYData[(n+2)*k]),  w,n);
				if(temp1<= 0) temp1=-(XYData[(n+2)*k + n+1] - WAM(&(XYData[(n+2)*k]),  w,n));
				if(temp1<0) temp1=0;
				temp += temp1;
			}


		on<<"Difference predicted and observed "<<temp<<endl;

			on<<"This fuzzy measure is:"<<endl;
			on<<"- additive"<<endl;
	}

quit1:
	if(datanumtest>0) {
		inf1.open(filename_o1.c_str(), std::ios::in);
// we have read datanum pairs (x,y), with x a vector of size Dimension and y a scalar
// we keep them in the array XYData
		XYData.newsize(datanumtest*(n));
		k=0;
		for(i1=0;i1<datanumtest;i1++) {
			for(j1=0;j1<n;j1++) {
				inf1 >> XYData[(n)*i1 + j1];
				if(inf1.fail()) break;
			}
			if(inf1.fail()) break;
			k++;
		}
	   datanumtest=k;
	   on1.open(filename_o2.c_str(), std::ios::out);
	   for(i1=0;i1<datanumtest;i1++) {
		   if(symmetric) if(!additive) on1<<OWA(&(XYData[(n)*i1]),  w,n)<<endl;
		   else on1<<WAM(&(XYData[(n)*i1]),  w,n)<<endl;
		   else
			on1<<Choquet(&(XYData[(n)*i1]),  w2,n,m )<<endl;

	   }

	}

	delete[] interlow;
	delete[] interhigh;
	delete[] shapleylow;
	delete[] shapleyhigh;
	delete[] shapley;
	delete[] banzhaf;
	delete[] inter;
	delete[] interB;
	delete[] Mob;
	delete[] w;
	delete[] w2;

	delete[] dual;
	Cleanup_FM();

quit:
	on.close();
//	cin >> res;
	if(res==1)
	return 0; else 
	return 1; //some error detected

}



  /*	
	on.open(filename_o.c_str(), std::ios::out);

	on<<"Using fuzzy measure"<<endl;

	//on<<xData<<endl;

	// depending on cardordering, vData contains the values in cardinality or binary order
	// convert to binary ordering, our default ordering
	double temp;
	wData=vData; // just a copy
	if(cardordering) {
		for(i=0;i<m;i++) 
			vData[card2bit[i]] = wData[i];
	}
	
	for(i=0;i<m;i++)
		on<< vData[i] << " \t{"<<ShowValue(i)<<"}"<<endl;
//	on<<vData<<endl;

		Mobius(&vData[0],&wData[0],n,m);
	for(i=0;i<m;i++)
		on<< wData[i] << " \t{"<<ShowValue(i)<<"}"<<endl;

		Zeta(&wData[0],&vData[0],n,m);
	for(i=0;i<m;i++)
		on<< vData[i] << " \t{"<<ShowValue(i)<<"}"<<endl;
 
  // generate random data
	int k,KData=20;
	double *XYData=new double[KData*(n+1)];

	for(k=0;k<KData;k++) {
		for(i=0;i<n;i++) {
			XYData[(n+1)*k + i] = random();
		}
		XYData[(n+1)*k + n] =  Choquet(&(XYData[(n+1)*k]),  &vData[0],n,m );
	}
	ofstream dat;
	dat.open(filename.c_str(), std::ios::out);
	for(k=0;k<KData;k++) {
		for(i=0;i<=n;i++) {
			dat << XYData[(n+1)*k + i] << " ";
		}
		dat<<endl;
	}
	dat.close();
	*/
