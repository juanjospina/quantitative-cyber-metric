// fuzzymeasure.cpp : Defines the entry point for the console application.

#include <iostream>
#include <fstream>
using namespace std;
#include "../src/fuzzymeasuretools.h"


int main(int argc, char *argv[])
{
	unsigned int  i, j;
	int i1;
	ofstream on;
	
	int n=3;
	int_64 m;

	Preparations_FM(n,&m);

	double *x=new double[n];
	double *w=new double[m];
	double *Mob=new double[m];
	double *dual=new double[m];
	double *shapley=new double[n];
	double *banzhaf=new double[n];
	double *inter=new double[m];

	on.open("exampleoutput.txt", std::ios::out);

// specify some fuzzy measure
	w[0]=0;  w[1]=0.3; w[2]=0.4; w[3]=0.2;
	w[4]=0.75; w[5]=0.9; w[6]=0.6; w[7]=1;

	on<<"Using fuzzy measure"<<endl;
	for(i=0;i<m;i++)
		on<< w[i]<<" {"<<ShowValue(i) <<"} ";
	on<<endl;

	on<<"In cardinality ordering it would be "<<endl;
	for(i=0;i<m;i++)
		on<< w[bit2card[i]]<<" {"<<ShowValue(bit2card[i]) <<"} ";
	on<<endl;

	on<<endl;

	dualm(w, dual,n,m);
	on<<"dual measure "<<endl;
	for(i=0;i<m;i++)
		on<< dual[i]<<" {"<<ShowValue(i) <<"} ";
	on<<endl;

	on<<"Mobius transform "<<endl;
	Mobius(w, Mob,n,m);
	for(i=0;i<m;i++)
		on<< Mob[i]<<" {"<<ShowValue(i) <<"} ";
	on<<endl;

	on<<"Shapley values "<<endl;
    	Shapley(w, shapley,  n,  m);
	for(i1=0;i1<n;i1++) on<<shapley[i1]<<" ";
	on<<endl;

	on<<"Banzhaf values "<<endl;
	Banzhaf(w, banzhaf,  n,  m);
	for(i1=0;i1<n;i1++) on<<banzhaf[i1]<<" ";
	on<<endl;

	on<<"Interaction indices "<<endl;
	Interaction(Mob,inter,m);
	for(i=0;i<m;i++) on<<inter[i]<<" ";
	on<<endl;

	on<<"Entropy "<<Entropy(w,n,m)<<endl;
	on<<"Orness "<<Orness(Mob,n,m)<<endl;

	//x[0]=0.8; x[1]=0.5; x[2]=0.9; // Product a
	//x[0]=0.5; x[1]=0.8; x[2]=0.9; // Product b
	//x[0]=0.5; x[1]=0.8; x[2]=0.4; // Product c
	//x[0]=0.8; x[1]=0.5; x[2]=0.4; // Product d

	x[0]=shapley[0]; x[1]=shapley[1]; x[2]=shapley[2]; // Shapley values


	on<<"Choquet integral of "<<endl;
	for(i=0;i<n;i++) on<<x[i]<<" ";
	on<<endl;
	on<<"Ch(x)="<<Choquet(x,w,n,m)<<endl;

	on<<"Sugeno integral  "<<endl;
	on<<"Su(x)="<<Sugeno(x,w,n,m)<<endl;

	Cleanup_FM();
	on.close();

	delete[] w; // and the rest of the arrays
	delete[] x; // and the rest of the arrays
	delete[] Mob; // and the rest of the arrays
	delete[] dual; // and the rest of the arrays
	delete[] shapley; // and the rest of the arrays
	delete[] banzhaf; // and the rest of the arrays
	delete[] inter; // and the rest of the arrays

	return 0;
}

