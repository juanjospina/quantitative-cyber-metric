#!/bin/sh

# Author: Juan Ospina
# Contact: juanospinacasas@gmail.com

# Commands to remove files created by ./compile.sh (Clean compiled files)
# (Be careful when using this script, since it can delete other files.)

rm -r __pycache__
rm -r fmtools.py
rm -r _fmtools*
rm -r *.o
rm -r *.cxx
