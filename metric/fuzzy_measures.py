import numpy as np
import pandas as pd
import sympy as sym

def compute_fuzzymeasures_from_weights(weights, lambdaVal):
    """
    Computes fuzzy measures from expert weights.

    This function computes the corresponding fuzzy 
    measures g({x1,x2}), ... g({x1,x2,... x_n}) from
    the expert weights assigned.

    Parameters
    ----------
    weights : numpy_array (double)
        Array with weights (g1,...gn) assigned to each factor x: (x1,...xn)
        by experts.

    lambdaVal: double
        Lambda value used to compute fuzzy measures.

    Returns
    -------
    numpy_array (double)
        numpy array of all fuzzy measures. Its size is determined the number
        of factors x: (x1,...xn), so 2^n, where n is the number of factors.
        E.g., 5 factors -> (x1,x2,...,x5), then the size is 2^5 = 32 ->
        g({0}) = 0, g({x1}),... g({x1,x2,x3}), .... g({x1,x2,x3,x4,x5}) = 1.0

    """

    # ## Pattern to follow to add support for more combinations
    # 1. Create empty np.array to store respective extra combinations.  E.g.:
    #    fuzzyMeasCURRENT = np.array([])...
    # 2. Add new for loop as follows: 
    #   for _d in range(len(weights)-_c-_b-_a-(numberOfloops-2)):
    #       ...........
    # 3. Inside each loop compute: 
    #     productMultCURRENT = productMultPREVIOUS * (1+lambdaVal*weights[_d+_c+_b+_a+(numberOfloops-2)])
    #     fuzzyM_temp = _FUZZYM_computeTempFM(productMultCURRENT,lambdaVal)
    #     fuzzyMeasCURRENT = np.append(fuzzyMeasCURRENT, fuzzyM_temp)
    # 4. Outside all loops append the np.array to the fuzzy measures arrays after previous appends:
    #     fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasCURRENT)
    

    # Arrays contaning groups of combinations
    fuzzyMeas = np.array([0.0]) # initiliaze and add fuzzy measure for empty set.
    fuzzyMeasTrios = np.array([])
    fuzzyMeasQuartets = np.array([])
    fuzzyMeasQuintets = np.array([])
    fuzzyMeasSextets = np.array([])
    fuzzyMeasSeptets = np.array([])
    fuzzyMeasOctets = np.array([])

    # For loops computing all possible single combinations 
    for i in range(len(weights)):
        for j in range(len(weights)-i):
            if(i==0):
                productMult = (1+lambdaVal*weights[j])
            else:
                productMult = (1+lambdaVal*weights[i-1])*(1+lambdaVal*weights[i+j])
                for k in range(len(weights)-j-i-1):
                    productMultTrios = productMult * (1+lambdaVal*weights[k+j+i+1])
                    for l in range(len(weights)-k-j-i-2):
                        productMultQuartets = productMultTrios * (1+lambdaVal*weights[l+k+j+i+2])
                        for m in range(len(weights)-l-k-j-i-3):
                            productMultQuintets = productMultQuartets * (1+lambdaVal*weights[m+l+k+j+i+3])
                            for n in range(len(weights)-m-l-k-j-i-4):
                                productMultSextets = productMultQuintets * (1+lambdaVal*weights[n+m+l+k+j+i+4])
                                for p in range(len(weights)-n-m-l-k-j-i-5):
                                    productMultSeptets = productMultSextets * (1+lambdaVal*weights[p+n+m+l+k+j+i+5])
                                    for q in range(len(weights)-p-n-m-l-k-j-i-6):
                                        productMultOctets = productMultSeptets * (1+lambdaVal*weights[q+p+n+m+l+k+j+i+6])
                                    
                                        fuzzyM_temp = _FUZZYM_computeTempFM(productMultOctets,lambdaVal)
                                        fuzzyMeasOctets = np.append(fuzzyMeasOctets, fuzzyM_temp)

                                    fuzzyM_temp = _FUZZYM_computeTempFM(productMultSeptets,lambdaVal)
                                    fuzzyMeasSeptets = np.append(fuzzyMeasSeptets, fuzzyM_temp)

                                fuzzyM_temp = _FUZZYM_computeTempFM(productMultSextets,lambdaVal)
                                fuzzyMeasSextets = np.append(fuzzyMeasSextets, fuzzyM_temp)

                            fuzzyM_temp = _FUZZYM_computeTempFM(productMultQuintets,lambdaVal)
                            fuzzyMeasQuintets = np.append(fuzzyMeasQuintets, fuzzyM_temp)

                        fuzzyM_temp = _FUZZYM_computeTempFM(productMultQuartets,lambdaVal)
                        fuzzyMeasQuartets = np.append(fuzzyMeasQuartets, fuzzyM_temp)

                    fuzzyM_temp = _FUZZYM_computeTempFM(productMultTrios,lambdaVal)
                    fuzzyMeasTrios = np.append(fuzzyMeasTrios, fuzzyM_temp)

            fuzzyM_temp = _FUZZYM_computeTempFM(productMult,lambdaVal)
            fuzzyMeas = np.append(fuzzyMeas, fuzzyM_temp)
    
    # Append arrays to the overall fuzzyMeas array
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasTrios)
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasQuartets)
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasQuintets)
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasSextets)
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasSeptets)
    fuzzyMeas = np.append(fuzzyMeas,fuzzyMeasOctets)

    return fuzzyMeas



def _FUZZYM_computeTempFM(productMult,lambdaVal):
    """
    Helper fcn to perform fuzzy measure computation.

    This function performs the computation.

    Parameters
    ----------
    productMult : double
        Product summation (multiplication) computed from combinations.

    lambdaVal: double
        Lambda value used to compute fuzzy measures.

    Returns
    -------
    double
        final computation of fuzzy measure.
     

    """
    
    absProductMultMinusOne = np.abs(productMult-1.0)
    fuzzyM_temp = 1.0/np.abs(lambdaVal) * absProductMultMinusOne
    return fuzzyM_temp




def compute_lambdaVal(weights):
    """
    Computes Lambda from expert weights.

    This function computes the corresponding lambda value 
    from the expert weights assigned. The lambda value is 
    the root of the polynomial that is in the range:
    -1 <= lambdaVal < 0.

    The polynomial used is:
    lambdaVal + 1 = \product i=1 to n (1+lambdaVal*g_i)

    Parameters
    ----------
    weights : numpy_array (double)
        Array with weights (g1,...gn) assigned to each factor x: (x1,...xn)
        by experts.

    Returns
    -------
    double
        Lambda value used to compute fuzzy measures.

    """

    # Define symbolic variable, roots as only real
    x = sym.symbols('x', real=True)

    # Create symbolic expression of full equation
    expr_full = (1+x*weights[0]) # Initial function expression
    for w in range(len(weights)-1):
        expr_full = sym.Mul(expr_full, (1+x*weights[w+1]))
        
    # Solve (find roots) for equation    
    roots = sym.solve(sym.Eq(expr_full, x+1), x)

    # Extrac the lambda value from all roots
    lambdaVal = 0
    for root in roots:
        if(root >= -1.0 and root < 0):
            lambdaVal = root
    
    return lambdaVal