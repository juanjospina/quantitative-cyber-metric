import numpy as np
import pandas as pd

CVSS_3 = {'AV':{'Network':0.85,'Adjacent':0.62,'Local':0.55,'Physical':0.2},
          'PR':{'None':0.85,'Low':0.62,'High':0.27}, 
          'AC':{'Low':0.77,'High':0.44}, 
          'UI':{'None':0.85,'Required':0.62}, 
          'E': {'High':1.0,'Functional':0.97,'PoC':0.94,'Unproven':0.91}}


def compute_qcrBase(cvssMetricValues):
    """
    Calculate the Base Quantitative Cyber Risk (QCR) for individual cyber nodes.

    This function calculates the QCR(Base) for the node 'dictionary' passed in.

    Parameters
    ----------
    cvssMetricValue : Array with cvss scores dictionaries of each bus in the system.
        
    Returns
    -------
    double
        Probability QCR(Base) values for each node/bus. Impact will be computed outside.
        QCR = Probability x Impact
    """
    
    probQCR = CVSS_3['AV'][cvssMetricValues['AV']] * CVSS_3['PR'][cvssMetricValues['PR']] \
            * CVSS_3['AC'][cvssMetricValues['AC']] * CVSS_3['UI'][cvssMetricValues['UI']] \
            * CVSS_3['E'][cvssMetricValues['E']]
    return probQCR


