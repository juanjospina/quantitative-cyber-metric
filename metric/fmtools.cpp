/**
 * @file fmtools.cpp
 *
 * @brief Provide functions to be interfaced with Python.
 * 
 * @defgroup fmtools fmtools_python_interface
 *
 * @author Juan Ospina
 * Contact: juanospinacasas@gmail.com
 *
 */


#include <iostream>
#include <math.h>
#include "fmtools.h" 								//Header file
#include "../fmtools_3.0/src/fuzzymeasuretools.h"	// fmtoolbox 

/**
    Calculates the Choquet integral of x_in using w_in.

    @param n number of players.
	@param x_in values observed.
	@param w_in fuzzy measures estimated.
    @return The Choquet integral value.
*/
double choquetIntegral(int n, std::vector<double> x_in, std::vector<double> w_in)
{
	unsigned int i_iter;	// iteration counter	
	int_64 m; 				// 2^n
	double ci_result = 0;	// result from Choquet integral calculation

	// Prepare local variables to be used in fcns fmtoolbox
	Preparations_FM(n,&m);	
	
	// Local variables to be passed into fmtoolbox fcns
	double *x=new double[n];
	double *w=new double[m];

	// Assign x_in to x.
	i_iter = 0;
	for (std::vector<double>::iterator it = x_in.begin(); it!=x_in.end(); it++)
	{
		x[i_iter] = *it;
		i_iter++;
	}

	// Assign w_in to w.
	i_iter = 0;
	for (std::vector<double>::iterator it = w_in.begin(); it!=w_in.end(); it++)
	{
		w[i_iter] = *it;
		i_iter++;
	}

	// Calculate Choquet Integral
	ci_result = Choquet(x,w,n,m);

	// Cleanup
	Cleanup_FM();
	delete[] w;
	delete[] x;

	return ci_result;

}


/**
    Calculates the Interaction indices & Mobious transform from w_in.

    @param n number of players.
	@param w_in fuzzy measures estimated.
    @return The Interaction indices.
*/
std::vector<double> interactionIndexAndMobius(int n, std::vector<double> w_in)
{

	unsigned int i_iter;
	unsigned int i_iter2;	
	int_64 m; // 2^n
	std::vector<double> interaction_indices; // output vector

	// Prepare local variables to be used in fcns fmtoolbox
	Preparations_FM(n,&m);

	// Local variables to be passed into fmtoolbox fcns
	double *w=new double[m];
	double *Mob=new double[m];
	double *inter=new double[m];

	// Assign w_in to w.
	i_iter = 0;
	for (std::vector<double>::iterator it = w_in.begin(); it!=w_in.end(); it++)
	{
		w[i_iter] = *it;
		i_iter++;
	}


	//Calculate Mobious transform
	Mobius(w,Mob,n,m);
	
	// for(i=0;i<m;i++)
	// 	std::cout<<"Mob: "<<Mob[i]<<", ";
	// std::cout<<std::endl;
	

	// Calculate Interaction Indices
	Interaction(Mob,inter,m);

	// Copy values to output vector
	for(i_iter2=0;i_iter2<m;i_iter2++)
		interaction_indices.push_back(inter[i_iter2]);

	// Cleanup
	Cleanup_FM();
	delete[] w;
	delete[] Mob;
	delete[] inter;

	return interaction_indices;
}


/**
    Calculates the Shapley values from w_in.

    @param n number of players.
	@param w_in fuzzy measures estimated.
    @return The Shapley values of each player.
*/
std::vector<double> shapleyValues(int n, std::vector<double> w_in)
{
	
	unsigned int i_iter;
	unsigned int i_iter2;	
	int_64 m; // 2^n
	std::vector<double> shapley_values; // output vector

	// Prepare local variables to be used in fcns fmtoolbox
	Preparations_FM(n,&m);	

	// Local variables to be passed into fmtoolbox fcns
	double *w=new double[m];
	double *shapley=new double[n];


	// Assign w_in to w.
	i_iter = 0;
	for (std::vector<double>::iterator it = w_in.begin(); it!=w_in.end(); it++)
	{
		w[i_iter] = *it;
		i_iter++;
	}

	// Calculate Shapley Values
	Shapley(w,shapley,n,m);
	
		// Copy values to output vector
	for(i_iter2=0;i_iter2<n;i_iter2++)
		shapley_values.push_back(shapley[i_iter2]);

	// Cleanup
	Cleanup_FM();
	delete[] w;
	delete[] shapley;

	return shapley_values;
}

