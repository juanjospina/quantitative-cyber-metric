import math
import numpy as np
import pandas as pd
from numpy.linalg import inv # invert matrices
import pandapower as pp      # run 1P1Q in crpi factor




def compute_vdi(voltsPu):
    """
    Calculate Voltage Deviation Index (VDI)

    This function calculates the VDI for multiple buses
    passed as inputs in a numpy array.

    Parameters
    ----------
    voltsPu : numpy_array (double)
        Voltages (in pu) for all buses in the system or zone.

    Returns
    -------
    numpy_array (double)
        numpy array of VDI values for each bus in the array given.

    """
    vdiBuses = np.absolute(voltsPu - 1.0)
    return vdiBuses


def compute_svsi(voltsPu, Ym, m, n, genBusIndeces):
    """
    Calculate Simplified Voltage Stability Index (SVSI)

    This function calculates the SVSI for multiple buses
    passed as inputs in a numpy array.

    Parameters
    ----------
    voltsPu : numpy_array (double)
        Voltages (in pu) for all buses in the system or zone.

    Ym: Series.to_dense()
        Admittance matrix for the system.

    m: double
        number of generator buses (PV buses) + slack in the system

    n: double
        number of load buses (PQ buses)

    genBusIndeces: numpy array (double)
        array with bus index where generators (PV buses) are located.

    Returns
    -------
    numpy_array (double)
        numpy array of SVSI values for each bus in the array given.

    """
      
    # compute the max difference of voltage magnitudes 
    # between any two buses in the system.
    maxVolt = np.amax(np.absolute(voltsPu)) # gets the max value
    minVolt = np.amin(np.absolute(voltsPu)) # gets the min value
    # compute beta
    beta = 1.0 - (((maxVolt-minVolt))**2)


    # compute relative electrical distance (RED)
    # Ymatrix = [Y_gg   Y_gl]
    #           [Y_lg    Y_ll]
    # Y_gg E C^mxm
    # Y_ll E C^nxn
    # Y_lg = Y_gl E Cnxm

    N = len(Ym)   # total number of buses in the system

    # Extract respective sections of Ymatrix
    Y_lg = Ym[N-n:,0:m]
    Y_ll = Ym[N-n:,N-n:]
    
    # Invert Y_ll
    Y_ll_inv = inv(Y_ll)

    # Calculate F_lg
    F_lg =-np.dot(Y_ll_inv,Y_lg) 

    # Create A matrix
    sizeOfA = (n,m)
    Amatrix = np.ones(sizeOfA)
    
    # Compute relative electrical distances (RED) matrix
    # Interpretation: Load(rows), Gens (Columns)
    # The smallest value in the row tells you which Gen is closer to that load (row)
    R_lg = Amatrix - np.absolute(F_lg)   

    # Use R_lg to find the closest generator to each load.
    # Do for loop to select the voltage pu value of the closest Gen to each bus
    vgenClosest = []
    loadCounter = 0
    for i in range(N):
        # if it is a gen bus, append the same v_pu
        if (np.isin(i,genBusIndeces)):
            vgenClosest = np.append(vgenClosest,voltsPu[i]) 
        # else if it is a load bus, use R_lg to find closer gen and get v_pu from it
        else:
            rowExtracted = R_lg[loadCounter,:] # extract the row for 
            closestGenIndex = np.argmin(rowExtracted)
            vgenClosest = np.append(vgenClosest,voltsPu[closestGenIndex]) 
            loadCounter = loadCounter + 1
    
    deltaV = np.absolute(vgenClosest - voltsPu)
    svsiBuses = np.divide(deltaV, (np.multiply(beta,voltsPu)))

    return svsiBuses




def compute_vcpi(voltsPu, Ym):
    """
    Calculate Voltage Collapse Prediction Index (VCPI)

    This function calculates the VCPI for multiple buses
    passed as inputs in a numpy array.

    Parameters
    ----------
    voltsPu : numpy_array (double)
        Voltages (in pu) for all buses in the system or zone.

    Ym: Series.to_dense()
        Admittance matrix for the system.

    Returns
    -------
    numpy_array (double)
        numpy array of VCPI values for each bus in the array given.

    """
      
    N = len(Ym)   # total number of buses in the system

    # Array to store the VCPI value for each node in the network
    vcpiBuses = []

    # loop over all buses
    for k in range(N):
        # calculate sum_of_vmHat
        vmHat = 0                          # clean vmHat for next bus
        sumYkj = np.sum(Ym[k,:]) - Ym[k,k] # compute sum_of_Ykj
       
        for m in range(N):
            # Add all vmHats only if m is not equal to k
            if (m != k):
                # compute vmHat
                vmHat = vmHat + (Ym[k,m]/sumYkj)*voltsPu[m]

        vcpi_k = np.absolute(1-(vmHat/voltsPu[k]))
        vcpiBuses = np.append(vcpiBuses, vcpi_k)

    return vcpiBuses



def compute_fdf(freqs, percentLimit=0.005, nomFreq=60):
    """
    Calculate Frequency Deviation Factor (FDF)

    This function calculates the FDF for multiple buses frequencies
    passed as inputs in a numpy array.

    Parameters
    ----------
    freqs : numpy_array (double)
        Frequencies (in Hz) for all buses in the system or zone.

    percentLimit: double 
        Maximum percentage limit deviation allowed for frequency - Default: 0.005 (or 0.5%) 

    nomFreq: double
        Nominal frequency of the system (e.g., 50 or 60 Hz) - Default: 60

    Returns
    -------
    numpy_array (double)
        numpy array of FDF values for each bus in the array given.

    """
    
    fdfBuses = 1 - (np.divide((np.absolute(nomFreq-freqs)), (nomFreq*percentLimit)))
    fdfBuses = np.clip(fdfBuses, 0, 2)   # Clip negative values to zero.

    return fdfBuses



def compute_crpi(pp_net, nPIflow):
    """
    Calculate Contigency Ranking Performance Index (CRPI)

    This function calculates the CRPI for multiple buses based on 
    1P1Q contingency ranking method.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. The network is needed
        to extract line values and runfast-decoupled power 
        flow (FDF) 1P1Q process.

    nPIflow: int
        Integer value used to power the division between the current
        power flow in line and max. limit power flow in line.

    Returns
    -------
    numpy_array (double)
        numpy array of CRPI values for each bus in the array given.

    """
    
    # get number of buses in the system
    N = len(pp_net.bus.name.to_numpy())
  
    ## ---- convert line ratings from Amps to three-phase MW = I*V*sqrt(3) ------
    # Make variables that store a deep copy of the dataframes to use and merge
    busDataf = pp_net.bus.copy(deep=True)   
    lineDataf = pp_net.line.copy(deep=True)

    # change name of column 'name' to 'from_bus' in 'bus' dataframe
    busDataf = busDataf.rename(columns={"name": "from_bus"})
    # print(busDataf)

    # Merge dataframes based on 'from_bus' column
    # "This is performed with the objevtive of getting the vn_kv values for the lines based on the from_bus"
    mergedDatafs = pd.merge(busDataf, lineDataf, on='from_bus')
    # print(mergedDatafs)

    maxLineRatingsMW = pp_net.line.max_i_ka.to_numpy()*math.sqrt(3)*mergedDatafs.vn_kv.to_numpy()
    numOfLines = pp_net.line.shape[0] # Get the total number of lines
    # print(maxLineRatingsMW)
    #-----------------------------------------------------------------------------------

    # Make a deep copy of the original (before contingency) lines dataframe 
    lineOriginal_pp_net = pp_net.line.copy(deep=True)

    # Performance Index (PI) values np array
    piResultArray = np.array([])


    for caseLine in range(numOfLines):

        # copy back original line dataframe
        pp_net.line = lineOriginal_pp_net.copy(deep=True)

        # remove line (i.e., contingency case where line: caseLine is lost)
        pp_net.line.drop([caseLine], inplace=True)      # drop row with Line connection
        pp_net.line.reset_index(drop=True,inplace=True) # reset index

        # Run 1 iteration of Fast-decoupled PF (i.e., 1P1Q)
        # 1P1Q - 1 iteration with big tolerance 1e-1 MVA using FDBX - tolerance_mva=1e-1
        # pp.diagnostic(pp_net)
        pp.runpp(pp_net, algorithm="fdxb", max_iteration=2, init="flat", tolerance_mva=1e-1) # using 'flat' fixed the problem of convergence.
        # print("****** Debug: Print FDBX Results - Case Line: ", caseLine, " ********")
        # print(pp_net.res_bus)   # PF results for buses.
        # print(pp_net.res_line)  # PF results for lines.
        # print("************************************************")
        pFlowLine = pp_net.res_line.p_from_mw.to_numpy()    # Get Line flows calculated from 1P1Q
        maxLineRatingsMW_caseLine =  np.delete(maxLineRatingsMW, caseLine) # Delete corresponding Max. limit from MW limit array
        pDivision = np.divide(pFlowLine, maxLineRatingsMW_caseLine)  # Divide Line flows over Max Line limits
        pPowernPIflow = np.power(pDivision, 2*nPIflow)      # Power ^2nPIflow each value calculated
        piValue = np.sum(pPowernPIflow)                     # Sum all elements to calculate performance index (PI)
        piResultArray = np.append(piResultArray, piValue)   # Add PI value to array that stores all PIs for all outage cases

    # Sort Contingencies
    sortedPiResults = pd.concat([lineOriginal_pp_net["from_bus"], lineOriginal_pp_net["to_bus"]], axis=1)
    sortedPiResults['PI_results'] = piResultArray
    sortedPiResults.sort_values(by=['PI_results'], inplace=True, ascending=False)
    # print(sortedPiResults)

    # Initialize result vector with zeros
    crpiBuses = np.zeros(N)

    # Go through sorted dataframe and assign the max PI value to corresponding connected buses
    for index, row in sortedPiResults.iterrows():
        if(row['PI_results'] > crpiBuses[int(row['from_bus'])]):
            crpiBuses[int(row['from_bus'])] = row['PI_results']

        if(row['PI_results'] > crpiBuses[int(row['to_bus'])]):
            crpiBuses[int(row['to_bus'])] = row['PI_results']


    return crpiBuses
