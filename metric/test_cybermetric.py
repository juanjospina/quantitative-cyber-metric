import pandapower as pp
import pandapower.plotting as plot
import pandapower.converter as cv
import math
import numpy as np
import pandas as pd
import factors_electrical as fe
import factors_graphtheory as fg
import factors_capability as fc
import factors_it as fit
import fuzzy_measures as fm
# To import fmtools we need to first run ./compile.sh
# Then, to remove the compiled unnecesary files we need to run ./remove_compiled_files.sh
import fmtools


# Convert MATPOWER Case file to PandaPower Case file
pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case6ww.mat', f_hz=60)
# pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case24_ieee_rts.mat', f_hz=60)


# Print all information about the imported power system 
# print(pp_net)

# Each dataframe in the object contains respective info.
# print(pp_net.bus)   # bus info.
# print(pp_net.line)  # line info.
# print(pp_net.trafo) # transformer info.
# print(pp_net.load)  # load info.
# print(pp_net.gen)   # Gens. info.
# print(pp_net.ext_grid)   # Slack Gens. info.


# Assign expert weights for each factor to consider (0.0 to 1.0)
# Number of players(factors/metrics)
numOfFactors = 5
# FACTORS:
# Important: The order matters! for importance check the Shapley values
# CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI
weights = [0.26,0.46,0.61,0.65,0.55]
lambdaVal = fm.compute_lambdaVal(weights)
fuzzyMeas = fm.compute_fuzzymeasures_from_weights(weights, lambdaVal)
print("Lambda Value is: {}".format(lambdaVal))
print("The fuzzy measures for factors are: {}".format(fuzzyMeas))
print("The total number of fuzzy measure combinations is: {}".format(len(fuzzyMeas)))

# Initialize inputs as vector template types (defined in fmtools.i)
x = fmtools.Vector()               # x: given observed values
fuzzyWeights = fmtools.Vector()    # w: estimated fuzzy measures

# Assign to each index the measure (Needs to be done manually)
for i in range(len(fuzzyMeas)):
    _tempVal = float(fuzzyMeas[i])
    print(_tempVal)
    fuzzyWeights.push_back(_tempVal)


# Max. P available from all gens = 200+150+180 = 530 MW
# Loads 3, 4, 5 (initial 100 MW all)
# For loop for executing Time series PF.
n_timesteps = 24
for timestep in range(n_timesteps):

    # reload original case network (This avoids some convergence issues caused by CRPI calc)
    pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case6ww.mat', f_hz=60)

    b3_load = (np.random.random(1) + 0.2) * 100
    b4_load = (np.random.random(1) + 0.2) * 100
    b5_load = (np.random.random(1) + 0.98) * 90

    print(b3_load)
    print(b4_load)
    print(b5_load)
    pp_net.load.loc[pp_net.load['bus'] == 3, 'p_mw'] = int(round(b3_load[0]))
    pp_net.load.loc[pp_net.load['bus'] == 4, 'p_mw'] = int(round(b4_load[0]))
    pp_net.load.loc[pp_net.load['bus'] == 5, 'p_mw'] = int(round(b5_load[0]))

    # Run Power Flow (To get states: can also be done through SE)
    pp.runpp(pp_net)
    # print("************ Print ACPF Resutls ****************")
    # print(pp_net.res_bus)   # PF results for buses.
    # print(pp_net.res_line)  # PF results for lines.
    # print("************************************************")


    # get admittance matrix from internal data structures
    Ymatrix  = pp_net._ppc["internal"]["Ybus"].todense()
    # Ymatrix  = pp_net._ppc["internal"]["Ybus"]

    # count the number of generators = num of gens + num of slack bus
    numOfGens = len(pp_net.gen.index) + len(pp_net.ext_grid.index)
    numOfLoads = len(pp_net.load.index) 
    # extract the bus numbers where gens and slack bus are into an array.
    genBusIndeces = pp_net.gen.bus.to_numpy()
    genBusIndeces = np.append(genBusIndeces,pp_net.ext_grid.bus.to_numpy())


    #----------------------------------------------------------

    # ================== VDI Factor ====================
    vdiFactors = fe.compute_vdi(pp_net.res_bus.vm_pu.to_numpy())
    print("VDI Factors: ", vdiFactors)

    # ================== SVSI Factor ====================
    svsiFactors = fe.compute_svsi(pp_net.res_bus.vm_pu.to_numpy(), Ymatrix, numOfGens, numOfLoads, genBusIndeces)
    print("SVSI Factors: ", svsiFactors)

    # ================== VCPI Factor ====================
    vcpiFactors = fe.compute_vcpi(pp_net.res_bus.vm_pu.to_numpy(), Ymatrix)
    print("VCPI Factors: ", vcpiFactors)

    # # ================== FDF Factor ==================== (cannot be tested yet since frequency measurements are needed!)
    # measFrequencies = np.array([59.8,60.1,60.0,60.0,60.0,60.0])
    # fdfFactors = fe.compute_fdf(measFrequencies)
    # print("FDF Factors: ", fdfFactors)

    # ===================== CRPI Factor ==========================
    # Parameters for Cont. Ranking
    nPIflow = 5
    voltageBase = 230 # in kV
    crpiFactors = fe.compute_crpi(pp_net, nPIflow)
    maxCRPIValue = np.amax(crpiFactors)    # max crpi value of all nodes 
    crpiFactors = crpiFactors/maxCRPIValue # normalize all values based on max. i.e., 1.0 for the most vulnerable 
    print("CRPI Factors: ", crpiFactors)

    # ===================== Closeness Centrality (CL) Factor ==========================
    clFactors = fg.compute_cl(pp_net)
    print("CL Factors: ", clFactors)


    # ===================== Edge Betweenness Centrality (EBC) Factor ==========================
    ebcFactors = fg.compute_ebc(pp_net)
    print("EBC Factors: ", ebcFactors)


    # ===================== Betweenness Centrality (BC) Factor ==========================
    bcFactors = fg.compute_bc(pp_net)
    print("BC Factors: ", bcFactors)


    # ===================== Repetition of Sources (RS) Factor ==========================
    # rsFactors = fc.compute_rs(pp_net)
    # print("RS Factors: ", rsFactors)

    # ===================== Base Quantitative Cyber Risk (QCR) Factor ==========================
    # N = len(pp_net.bus.name.to_numpy())
    b1 = {'AV': 'Network', 'PR': 'Low', 'AC': 'Low', 'UI': 'None', 'E': 'High'}
    b2 = {'AV': 'Adjacent', 'PR': 'Low', 'AC': 'High', 'UI': 'Required', 'E': 'High'}
    b3 = {'AV': 'Local', 'PR': 'None', 'AC': 'Low', 'UI': 'None', 'E': 'Functional'}
    b4 = {'AV': 'Physical', 'PR': 'Low', 'AC': 'High', 'UI': 'Required', 'E': 'High'}
    b5 = {'AV': 'Network', 'PR': 'High', 'AC': 'Low', 'UI': 'None', 'E': 'Unproven'}
    b6 = {'AV': 'Adjacent', 'PR': 'Low', 'AC': 'High', 'UI': 'Required', 'E': 'PoC'}

    qcrFactors = [fit.compute_qcrBase(b1),fit.compute_qcrBase(b2), \
                fit.compute_qcrBase(b3),fit.compute_qcrBase(b4),fit.compute_qcrBase(b5), \
                fit.compute_qcrBase(b6)]
                
    print("QCR Factors: ", qcrFactors)
    print("------------------------------------------------------------------------")

#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
# --------------------------- Tests with Time Series Power Flow ---------------------------------------

    # Loops for  fuzzy measures
    numNodes = len(pp_net.bus.name.to_numpy())
    for numNode in range(numNodes):
        qcrRisk = (clFactors[numNode]+ebcFactors[numNode]+bcFactors[numNode])*qcrFactors[numNode]
        # CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI
        x = [crpiFactors[numNode], qcrRisk, vdiFactors[numNode], svsiFactors[numNode], vcpiFactors[numNode]]
        print("measured factors for node {}: {}".format(numNode,x))
        res_ci = fmtools.choquetIntegral(numOfFactors,x,fuzzyWeights)
        print("Choquet Integral for node {}: {}".format(numNode,res_ci))
    print("_________________________________________________________________________")


# Compute Interaction Indeces, Mobius Transform and Shapley Values for All.
res_inter = fmtools.interactionIndexAndMobius(numOfFactors,fuzzyWeights)
print("Interaction Indeces and Mobius: {}".format(res_inter))
res_shapley = fmtools.shapleyValues(numOfFactors,fuzzyWeights)
print("Shapley Values: {}".format(res_shapley))


    # # Loops for writing to files and then fit them to get fuzzy measures
    # numNodes = len(pp_net.bus.name.to_numpy())
    # for numNode in range(numNodes):
    #     path = "./bus_{}".format(numNode)
    #     lineWrite = "{} {} {} {} {}\n".format(vdiFactors[numNode],svsiFactors[numNode], \
    #          vcpiFactors[numNode],crpiFactors[numNode], \
    #          (clFactors[numNode]+ebcFactors[numNode]+bcFactors[numNode])*qcrFactors[numNode])
    #     with open(path, 'a') as out_file:
    #         out_file.write(lineWrite)

# ////////////////////// Draw Network Graph ////////////////////////////////////////
fileName = "network_drawed.png"
fg.draw_network(pp_net, fileName)
