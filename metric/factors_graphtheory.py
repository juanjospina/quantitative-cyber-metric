import pandapower.topology as top
import networkx as nx
import pandapower as pp 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def compute_bc(pp_net):
    """
    Calculate Betweenness Centrality (BC) of network graph nodes.

    This function calculates the BC for the network topology 
    given.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. 

    Returns
    -------
    numpy_array (double)
        numpy array with BC values for each node/bus.

    """

    # get number of buses in the system
    N = len(pp_net.bus.name.to_numpy())
    bcBuses = np.arange(N)

    # create networkx network
    networkTopology = top.create_nxgraph(pp_net)
    # convert to digraph
    networkTopologyDigraph = nx.DiGraph(networkTopology)
    # calculate closeness centrality
    bc_dict = nx.betweenness_centrality(networkTopologyDigraph)

    # vectorize results (from dict to numpy array)
    bcBuses = np.vectorize(bc_dict.get)(bcBuses)

    return bcBuses



def compute_cl(pp_net):
    """
    Calculate Closeness Centrality (CL) of network graph nodes.

    This function calculates the CL for the network topology 
    given.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. 

    Returns
    -------
    numpy_array (double)
        numpy array with CL values for each node/bus.

    """

    # get number of buses in the system
    N = len(pp_net.bus.name.to_numpy())
    clBuses = np.arange(N)

    # create networkx network
    networkTopology = top.create_nxgraph(pp_net)
    # calculate closeness centrality
    cl_dict = nx.closeness_centrality(networkTopology)
    # vectorize results (from dict to numpy array)
    clBuses = np.vectorize(cl_dict.get)(clBuses)

    return clBuses



def compute_ebc(pp_net):
    """
    Calculate Edge Betweenness Centrality (EBC) of network graph.

    This function calculates the EBC for the network topology 
    given.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. 

    Returns
    -------
    numpy_array (double)
        numpy array with BC values for each node/bus.
        The max. EBC value for an edge connected to the 
        respective node/bus.

    """

      # get number of buses in the system
    N = len(pp_net.bus.name.to_numpy())
    ebcBuses = np.zeros(N)

    # create networkx network
    networkTopology = top.create_nxgraph(pp_net)
    # calculate edge betweenness centrality
    ebc_dict = nx.edge_betweenness_centrality(networkTopology)

    # Go through sorted dataframe and assign the max PI value to corresponding connected buses
    for key in ebc_dict:
        # print(key, '->', ebc_dict[key])
        # Go through dict and assign the max ebc value to corresponding connected buses
        if(ebc_dict[key] > ebcBuses[key[0]]):
            ebcBuses[key[0]] = ebc_dict[key]
        if(ebc_dict[key] > ebcBuses[key[1]]):
            ebcBuses[key[1]] = ebc_dict[key]
    
    return ebcBuses



def draw_network(pp_net, fileName):
    """
    Draws the network graph in a png file.

    This function draws the network topology 
    of the network given.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. 

    fileName : str
        Name of the file where the graph is going to be drawn. 

    Returns
    -------
    None

    """

    # create networkx network
    networkTopology = top.create_nxgraph(pp_net)
    # draw the network
    nx.draw_networkx(networkTopology, arrows=True, with_labels=True)
    # plot and save in file
    plt.savefig(fileName)
