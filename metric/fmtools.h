/**
 * @file fmtools.h
 *
 * @brief Header file for fmtools.cpp
 * 
 * @defgroup fmtools fmtools_python_interface
 *
 * @author Juan Ospina
 * Contact: juanospinacasas@gmail.com
 *
 */

#include <vector>

double choquetIntegral(int n, std::vector<double> x_in, std::vector<double> w_in);
std::vector<double> interactionIndexAndMobius(int n, std::vector<double> w_in);
std::vector<double> shapleyValues(int n, std::vector<double> w_in);

