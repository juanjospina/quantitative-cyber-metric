/* Interface Module:  C++ to Python
   Author: Juan Ospina
   Contact: juanospinacasas@gmail.com
*/


/* name of module to use*/
%module fmtools 


%{
        /* Every thing in this file is being copied in  
        wrapper file. We include the C header file necessary 
        to compile the interface */
        #include "fmtools.h"
%}

// Add the type of vector as Line or Array to namespace std
%include "std_vector.i" // Comes from SWIG library
namespace std
{
        %template(Vector) vector < double >;
        %template(Array) vector < vector < double > >;
}

/* explicitly list functions and variables to be interfaced */

/* or if we want to interface all functions then we can simply 
   include header file like this -  
*/
%include "fmtools.h"
