import numpy as np
import sympy as sym

# Define symbolic variable, roots as only real
x = sym.symbols('x', real=True)


# roots = solve(Eq(x**2, 1), x)
roots = sym.solve(sym.Eq((1+x*0.52)*(1+x*0.54)*(1+x*0.36)*(1+x*0.36)*(1+x*0.34), x+1), x)


print(roots)


lambdaVal = 0

for root in roots:
    if(root >= -1.0 and root < 0):
        lambdaVal = root

print(lambdaVal)