import fuzzy_measures as fm
# To import fmtools we need to first run ./compile.sh
# Then, to remove the compiled unnecesary files we need to run ./remove_compiled_files.sh
import fmtools


# Assign expert weights for each factor to consider (0.0 to 1.0)
# Number of players(factors/metrics)
numOfFactors = 5
# FACTORS:
# Important: The order matters! for importance check the Shapley values
# CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI, 
weights = [0.26,0.46,0.61,0.65,0.55]

lambdaVal = fm.compute_lambdaVal(weights)
fuzzyMeas = fm.compute_fuzzymeasures_from_weights(weights, lambdaVal)
print("Lambda Value is: {}".format(lambdaVal))
print("The fuzzy measures for factors are: {}".format(fuzzyMeas))
print("The total number of fuzzy measure combinations is: {}".format(len(fuzzyMeas)))

# Initialize inputs as vector template types (defined in fmtools.i)
x = fmtools.Vector()               # x: given observed values
fuzzyWeights = fmtools.Vector()    # w: estimated fuzzy measures

# Assign to each index the measure (Needs to be done manually)
for i in range(len(fuzzyMeas)):
    _tempVal = float(fuzzyMeas[i])
    print(_tempVal)
    fuzzyWeights.push_back(_tempVal)


# CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI, 
# Important: The order matters! for importance check the Shapley values
x = [0.1, 0.7, 0.1, 0.1 ,0.1]

print("TEST: measured factors: {}".format(x))
res_ci = fmtools.choquetIntegral(numOfFactors,x,fuzzyWeights)
print("TEST: Choquet Integral: {}".format(res_ci))

 
# Compute Interaction Indeces, Mobius Transform and Shapley Values for All.
res_inter = fmtools.interactionIndexAndMobius(numOfFactors,fuzzyWeights)
print("Interaction Indeces and Mobius: {}".format(res_inter))
res_shapley = fmtools.shapleyValues(numOfFactors,fuzzyWeights)
print("Shapley Values: {}".format(res_shapley))
