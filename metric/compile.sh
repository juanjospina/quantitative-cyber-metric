#!/bin/sh

# Author: Juan Ospina
# Contact: juanospinacasas@gmail.com

# Commands to be run when compiling and linking fmtools python interfacing files.
# This commands generate the object files.
swig -python -c++ fmtools.i
g++ -c -fpic ../fmtools_3.0/src/fuzzymeasuretools.cpp fmtools_wrap.cxx fmtools.cpp -I/usr/include/python3.6m
g++ -shared fuzzymeasuretools.o fmtools.o fmtools_wrap.o -o _fmtools.so

