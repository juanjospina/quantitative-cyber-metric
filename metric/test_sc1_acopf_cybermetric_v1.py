import pandapower as pp
import pandapower.plotting as plot
import pandapower.converter as cv
import math
import numpy as np
import pandas as pd
import factors_electrical as fe
import factors_graphtheory as fg
import factors_capability as fc
import factors_it as fit
import fuzzy_measures as fm
# To import fmtools we need to first run ./compile.sh
# Then, to remove the compiled unnecesary files we need to run ./remove_compiled_files.sh
import fmtools

# Import Plotly for interactive plots
from pandapower.plotting.plotly import pf_res_plotly
# pp.plotting.plotly.mapbox_plot.set_mapbox_token('pk.eyJ1Ijoic293b3dpZDQ1NSIsImEiOiJja2s0bTIzb3IxaG50MzFxZGZ1cXI1aDhrIn0.q07QI5gi7EKu-uwALopXHQ')

# Get GEO Data from original PandaPower Case
import pandapower.networks as pn
pp_net = pn.case24_ieee_rts()
geoData = pp_net.bus_geodata


# Convert MATPOWER Case file to PandaPower Case file
pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case24_ieee_rts.mat', f_hz=60)
# pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case6ww.mat', f_hz=60)


# Line Contingency 
LINE = 6


# ------------- Print all information about the imported power system ----------
# print(pp_net)
# Each dataframe in the object contains respective info.
print("All info. from Original Case")
# print(pp_net.bus)   # bus info.
print("\n Line Information: \n")
print(pp_net.line)  # line info.
# print(pp_net.trafo) # transformer info.
print("\n Load Information: \n")
print(pp_net.load)  # load info.
print("\n Gen Information: \n")
print(pp_net.gen)   # Gens. info.
# print(pp_net.ext_grid)   # Slack Gens. info.
print("---------------------------------------------------------------------------------")
# ---------------------------------------------------------------------------------

# # ------------------ Modify factors ----------------------
# Info: this could be useful to recreate attacking scenarios where traditional ACOPF is very high
# due to attack and CyberOPF is normal for CyberOPF.

# # Modify load values
# b3_load = (np.random.random(1) + 0.2) * 100
# pp_net.load.loc[pp_net.load['bus'] == 3, 'p_mw'] = int(round(b3_load[0]))

# # Modify generator values
# pp_net.gen.loc[pp_net.gen['bus'] == 3 , 'p_mw'] = int(round(b3_load[0]))
# #---------------------------------------------------------

# Optimal AC Power Flow
pp.runopp(pp_net)
# print(pp_net._ppc_opf)
print("Traditional ACOPF ACCost $: {}".format(pp_net.res_cost))
print("Traditional ACOPF Solution Generator Dispatch: \n {}".format(pp_net.res_gen))
print("Traditional ACOPF Solution Line Flows: \n {}".format(pp_net.res_line))

# --- Plot results from OPF
pp_net.bus_geodata = geoData    # Add coordinates and geo data to pp_net
pf_res_plotly(pp_net, aspectratio=(1,0.5), figsize=1.0, filename='results_acopf.html')

print("---------------------------------------------------------------------------------")

# remove line to cause contingency
# remove line (i.e., contingency case where line: caseLine is lost)
pp_net.line.drop([LINE], inplace=True)      # drop row with Line connection
pp_net.line.reset_index(drop=True,inplace=True) # reset index

pp.runopp(pp_net, init='results')
print("Traditional ACOPF Cost AFTER CONTINGENCY $: {}".format(pp_net.res_cost))
print("Traditional ACOPF Solution Generator Dispatch AFTER CONTINGENCY: \n {}".format(pp_net.res_gen))
print("Traditional ACOPF Solution Line Flows AFTER CONTINGENCY: \n {}".format(pp_net.res_line))

# --- Plot results from OPF
pp_net.bus_geodata = geoData    # Add coordinates and geo data to pp_net
pf_res_plotly(pp_net, aspectratio=(1,0.5), figsize=1.0, filename='results_acopf_after.html')
print("---------------------------------------------------------------------------------")


#--------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------

print("----------------- Run CyberOPF -------------------")

# Reload original net
pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case24_ieee_rts.mat', f_hz=60)

# # Run Power Flow (To get Ymatrix and Topology -> OPF result does not produces these data structures)
pp.runpp(pp_net)
Ymatrix  = pp_net._ppc["internal"]["Ybus"].todense() # Ymatrix will not change with OPF solution, so PF can be used.
# count the number of generators = num of gens + num of slack bus
numOfGens = len(pp_net.gen.index) + len(pp_net.ext_grid.index) 
# count the number of load buses
numOfLoads = len(pp_net.load.index)
# extract the bus numbers where gens and slack bus are into an array.
genBusIndeces = pp_net.gen.bus.to_numpy()
genBusIndeces = np.append(genBusIndeces,pp_net.ext_grid.bus.to_numpy())

# Assign expert weights for each factor to consider (0.0 to 1.0)
# Number of players(factors/metrics)
numOfFactors = 5
# FACTORS:
# Important: The order matters! for importance check the Shapley values
# CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI
weights = [0.26,0.46,0.61,0.65,0.55]
# weights = [0.46,0.61,0.65,0.55]

lambdaVal = fm.compute_lambdaVal(weights)
fuzzyMeas = fm.compute_fuzzymeasures_from_weights(weights, lambdaVal)
print("Lambda Value is: {}".format(lambdaVal))
print("The fuzzy measures for factors are: \n {}".format(fuzzyMeas))
print("The total number of fuzzy measure combinations is: \n {}".format(len(fuzzyMeas)))

# Initialize inputs as vector template types (defined in fmtools.i)
x = fmtools.Vector()               # x: given observed values
fuzzyWeights = fmtools.Vector()    # w: estimated fuzzy measures

# Assign to each index the measure (Needs to be done manually)
for i in range(len(fuzzyMeas)):
    _tempVal = float(fuzzyMeas[i])
    print(_tempVal)
    fuzzyWeights.push_back(_tempVal)

# ================== VDI Factor ====================
vdiFactors = fe.compute_vdi(pp_net.res_bus.vm_pu.to_numpy())
print("VDI Factors: \n ", vdiFactors)

# ================== SVSI Factor ====================
svsiFactors = fe.compute_svsi(pp_net.res_bus.vm_pu.to_numpy(), Ymatrix, numOfGens, numOfLoads, genBusIndeces)
print("SVSI Factors: \n ", svsiFactors)

# ================== VCPI Factor ====================
vcpiFactors = fe.compute_vcpi(pp_net.res_bus.vm_pu.to_numpy(), Ymatrix)
print("VCPI Factors: \n ", vcpiFactors)

# # ===================== CRPI Factor ==========================
# # Parameters for Cont. Ranking
nPIflow = 5
crpiFactors = fe.compute_crpi(pp_net, nPIflow)
maxCRPIValue = np.amax(crpiFactors)    # max crpi value of all nodes 
crpiFactors = crpiFactors/maxCRPIValue # normalize all values based on max. i.e., 1.0 for the most vulnerable 
print("CRPI Factors: \n ", crpiFactors)

# ===================== Closeness Centrality (CL) Factor ==========================
clFactors = fg.compute_cl(pp_net)
print("CL Factors: \n ", clFactors)


# ===================== Edge Betweenness Centrality (EBC) Factor ==========================
ebcFactors = fg.compute_ebc(pp_net)
print("EBC Factors: \n ", ebcFactors)


# ===================== Betweenness Centrality (BC) Factor ==========================
bcFactors = fg.compute_bc(pp_net)
print("BC Factors: \n ", bcFactors)


# # ===================== Base Quantitative Cyber Risk (QCR) Factor ==========================
N = len(pp_net.bus.name.to_numpy())
# Initialize
b = [{'AV': 'Network', 'PR': 'Low', 'AC': 'Low', 'UI': 'None', 'E': 'High'} for k in range(N)]
# Assign the specific QCR Factors
b[0]= {'AV': 'Physical', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[1]= {'AV': 'Physical', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[2] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[3] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[4] = {'AV': 'Physical', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[5] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[6] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[7] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[8] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[9] = {'AV': 'Network', 'PR': 'None', 'AC': 'Low', 'UI': 'None', 'E': 'Unproven'}
b[10] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[11] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[12] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[13] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[14] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[15] = {'AV': 'Network', 'PR': 'None', 'AC': 'Low', 'UI': 'None', 'E': 'Unproven'}
b[16] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[17] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[18] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[19] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[20] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[21] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[22] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}
b[23] = {'AV': 'Local', 'PR': 'High', 'AC': 'High', 'UI': 'Required', 'E': 'Unproven'}

qcrFactors = []
for i in range(N):
    qcrFactors.append(fit.compute_qcrBase(b[i]))

# print("QCR Factors: ", qcrFactors)
# print("------------------------------------------------------------------------")

# #------------------------------------------------------------------------------------------------------
# # --------------------------- Calculate CyberMetric for Each Node -------------------------------------

numNodes = len(pp_net.bus.name.to_numpy())
rho = 0.2  # rho is the max limit of Cybermetric
cyberMetricNodes = []   # List with cyber metrics for all nodes
problematicNodes = []   # List with index of nodes that have a cyberMetric higher than (>=) rho
for numNode in range(numNodes):
    qcrRisk = (clFactors[numNode]+ebcFactors[numNode]+bcFactors[numNode])*qcrFactors[numNode]
    # CRPI, QCR(EBC+CL+BC), VDI, SVSI, VCPI
    x = [crpiFactors[numNode], qcrRisk, vdiFactors[numNode], svsiFactors[numNode], vcpiFactors[numNode]]
    # x = [qcrRisk, vdiFactors[numNode], svsiFactors[numNode], vcpiFactors[numNode]]
    print("measured factors for node {}: {}".format(numNode,x))
    res_ci = fmtools.choquetIntegral(numOfFactors,x,fuzzyWeights)
    print("CyberMetric (CI) for node {}: {}".format(numNode,res_ci))
    cyberMetricNodes.append(res_ci)
    if(res_ci >= rho):
        problematicNodes.append(numNode)  # +1 is added to shift starting point from 0 t 1 # DOUBLE CHECK THIS!!!

print("_________________________________________________________________________")


print("Problematic Nodes: {}".format(problematicNodes))


# #------------------------------------------------------------------------------------------------------
# # --------------------------- Run CyberConstrained OPF -------------------------------------


# Modify ACOPF Formulation Limits (Min & Max - Fix) for problematic nodes.
# Reload original case
pp_net = cv.from_mpc('/home/juan/Documents/quantitative-cyber-metric/testcases/case24_ieee_rts.mat', f_hz=60)
# Print original data
print(pp_net.gen)

# pp_net.load.loc[pp_net.load['bus'] == 3, 'p_mw'] = int(round(b3_load[0]))

# If the problematic node is a generator node -> fix the generator max_p_mw = min_p_w
# If the problematic node is a load bus, then limit the lines that are connected to this node by X% amount?
#'loading_percent'
for i in range(len(problematicNodes)):
    # fix generator max as min
    pp_net.gen.loc[pp_net.gen['bus'] == problematicNodes[i] , 'max_p_mw'] = pp_net.gen.loc[pp_net.gen['bus'] == problematicNodes[i] , 'min_p_mw']
    # fix 'loading percent or rateA'
    # pp_net.gen.loc[pp_net.gen['bus'] == problematicNodes[i] , 'max_p_mw'] = pp_net.gen.loc[pp_net.gen['bus'] == problematicNodes[i] , 'min_p_mw']

# New Gen Data (Fixed)
print(pp_net.gen)

# pp_net.line["max_loading_percent"] = 86

# Run new OPF
# Optimal AC Power Flow
pp.runopp(pp_net)

print("CyberConstrained ACOPF Cost $: {}".format(pp_net.res_cost))
print("CyberConstrained ACOPF Solution Generator Dispatch: \n {}".format(pp_net.res_gen))
print("CyberConstrained ACOPF Solution Line Flows: \n {}".format(pp_net.res_line))
# print(pp_net)

#------ Plot results from OPF
pp_net.bus_geodata = geoData    # Add coordinates and geo data to pp_net
pf_res_plotly(pp_net, aspectratio=(1,0.5), figsize=1.0, filename='results_cyberopf.html')



# remove line to cause contingency
# remove line (i.e., contingency case where line: caseLine is lost)
pp_net.line.drop([LINE], inplace=True)      # drop row with Line connection
pp_net.line.reset_index(drop=True,inplace=True) # reset index

pp.runopp(pp_net, init='results')
print("CyberConstrained ACOPF Cost AFTER CONTINGENCY $: {}".format(pp_net.res_cost))
print("CyberConstrained ACOPF Solution Generator Dispatch AFTER CONTINGENCY: \n {}".format(pp_net.res_gen))
print("CyberConstrained ACOPF Solution Line Flows AFTER CONTINGENCY: \n {}".format(pp_net.res_line))

#------ Plot results After Contigency
pp_net.bus_geodata = geoData    # Add coordinates and geo data to pp_net
pf_res_plotly(pp_net, aspectratio=(1,0.5), figsize=1.0, filename='results_cyberopf_after.html')


