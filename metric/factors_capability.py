import pandapower.topology as top
import networkx as nx
import pandapower as pp 
import numpy as np
import pandas as pd


def compute_rs(pp_net):
    """
    Calculate Repetition of Sources (RS) for PQ buses/nodes.

    This function calculates the RS for each load (PQ) bus
    in the system using the networkx topology function.

    Parameters
    ----------
    pp_net : pandapower network (entire network structure)
        Pandapower network to study. 

    Returns
    -------
    numpy_array (double)
        numpy array with RS values for each node/bus.

    """


    # get number of buses in the system
    N = len(pp_net.bus.name.to_numpy())

    # count the number of generators = num of gens + num of slack bus
    totalNumOfGens = len(pp_net.gen.index) + len(pp_net.ext_grid.index)

    # extract the bus numbers where gens and slack bus are into an array.
    genBusIndeces = pp_net.gen.bus.to_numpy()
    genBusIndeces = np.append(genBusIndeces,pp_net.ext_grid.bus.to_numpy())

    # extract the bus numbers where loads are into an array.
    loadBusIndeces = pp_net.load.bus.to_numpy()

    # create networkx network
    networkTopology = top.create_nxgraph(pp_net)

    # array to store results
    rsBuses = np.zeros(N)    
    
    # Loop over each node/bus and find all nodes connected to it.
    for bus in range(N):
        genCounter = 0 # counter that counts the number of generators connected to the respective bus.
        # outputs a python generator with all the nodes that are connected to bus.
        connectedComp = top.connected_component(networkTopology, bus)
        # Loop over all the found nodes that are connected to the bus being evaluated
        for busConn in connectedComp:
            # Add 1 to generator counter if the bus is connected to a gen bus 
            if(np.isin(busConn, genBusIndeces)):
                genCounter = genCounter + 1

        # assign gen counter to respective value in array for the node
        rsBuses[bus] = genCounter

    # divide the number of gens calculated over the total nums of gens in the system
    rsBuses = np.divide(rsBuses, totalNumOfGens)
    return rsBuses


