# import fmtools
import fuzzy_measures as fm

# Number of players(metrics)
n = 3

# Weights assigned to each player/factor (metric)
# weights = [0.36,0.36,0.34,0.52,0.54,0.62,0.65,0.82]
# weights = [0.36,0.36,0.34,0.52,0.54,0.62,0.65]
# weights = [0.36,0.36,0.34,0.52,0.54, 0.62]
weights = [0.36,0.36,0.34,0.52,0.54]
# weights = [0.36,0.34,0.52,0.54]
# weights = [0.36,0.52,0.54]


lambdaVal = fm.compute_lambdaVal(weights)
fuzzyMeas = fm.compute_fuzzymeasures_from_weights(weights, lambdaVal)

print("Lambda Value is: {}".format(lambdaVal))
print("The fuzzy measures for factors are: {}".format(fuzzyMeas))
print("The total number of fuzzy measure combinations is: {}".format(len(fuzzyMeas)))


# # Initialize inputs as vector template types (defined in fmtools.i)
# a = fmtools.Vector()
# b = fmtools.Vector()

# # Inputs
# a = [0.8,0.5,0.9]                   # x: given observed values
# b = [0,0.3,0.4,0.2,0.75,0.9,0.6,1]  # w: estimated fuzzy measures

# # Choquet integral
# res_ci = fmtools.choquetIntegral(n,a,b)
# print(res_ci)

# # Interaction indices and Mobious transform
# res_inter = fmtools.interactionIndexAndMobius(n,b)
# print(res_inter)

# # Shapley Values
# res_shapley = fmtools.shapleyValues(n,b)
# print(res_shapley)
# print(res_shapley[1])


